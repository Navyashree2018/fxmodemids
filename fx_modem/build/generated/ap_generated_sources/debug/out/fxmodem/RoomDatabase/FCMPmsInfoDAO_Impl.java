package fxmodem.RoomDatabase;

import android.database.Cursor;
import androidx.room.EntityInsertionAdapter;
import androidx.room.RoomDatabase;
import androidx.room.RoomSQLiteQuery;
import androidx.room.SharedSQLiteStatement;
import androidx.room.util.CursorUtil;
import androidx.room.util.DBUtil;
import androidx.sqlite.db.SupportSQLiteStatement;
import fxmodem.Models.FCMPMSData.FCMPmsInfo;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;

@SuppressWarnings({"unchecked", "deprecation"})
public final class FCMPmsInfoDAO_Impl implements FCMPmsInfoDAO {
  private final RoomDatabase __db;

  private final EntityInsertionAdapter<FCMPmsInfo> __insertionAdapterOfFCMPmsInfo;

  private final SharedSQLiteStatement __preparedStmtOfDeleteAll;

  public FCMPmsInfoDAO_Impl(RoomDatabase __db) {
    this.__db = __db;
    this.__insertionAdapterOfFCMPmsInfo = new EntityInsertionAdapter<FCMPmsInfo>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR IGNORE INTO `fcm_pms_info` (`Date`,`Echo_Token`,`Notification_Type`,`Notification_Data`,`Notification_Date_Time`,`Pms_Request`,`Pms_Request_Data`,`Pms_Request_Date_Time`,`Pms_Response_Data`,`Pms_Response_Date_Time`,`FX_Request`,`FX_Request_Data`,`FX_Request_Date_Time`,`FX_Response_Data`,`FX_Response_Date_Time`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, FCMPmsInfo value) {
        if (value.getDate() == null) {
          stmt.bindNull(1);
        } else {
          stmt.bindString(1, value.getDate());
        }
        if (value.getEcho_Token() == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.getEcho_Token());
        }
        if (value.getNotification_Type() == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindString(3, value.getNotification_Type());
        }
        if (value.getNotification_Data() == null) {
          stmt.bindNull(4);
        } else {
          stmt.bindString(4, value.getNotification_Data());
        }
        if (value.getNotification_Date_Time() == null) {
          stmt.bindNull(5);
        } else {
          stmt.bindString(5, value.getNotification_Date_Time());
        }
        if (value.getPms_Request() == null) {
          stmt.bindNull(6);
        } else {
          stmt.bindString(6, value.getPms_Request());
        }
        if (value.getPms_Request_Data() == null) {
          stmt.bindNull(7);
        } else {
          stmt.bindString(7, value.getPms_Request_Data());
        }
        if (value.getPms_Request_Date_Time() == null) {
          stmt.bindNull(8);
        } else {
          stmt.bindString(8, value.getPms_Request_Date_Time());
        }
        if (value.getPms_Response_Data() == null) {
          stmt.bindNull(9);
        } else {
          stmt.bindString(9, value.getPms_Response_Data());
        }
        if (value.getPms_Response_Date_Time() == null) {
          stmt.bindNull(10);
        } else {
          stmt.bindString(10, value.getPms_Response_Date_Time());
        }
        if (value.getFX_Request() == null) {
          stmt.bindNull(11);
        } else {
          stmt.bindString(11, value.getFX_Request());
        }
        if (value.getFX_Request_Data() == null) {
          stmt.bindNull(12);
        } else {
          stmt.bindString(12, value.getFX_Request_Data());
        }
        if (value.getFX_Request_Date_Time() == null) {
          stmt.bindNull(13);
        } else {
          stmt.bindString(13, value.getFX_Request_Date_Time());
        }
        if (value.getFX_Response_Data() == null) {
          stmt.bindNull(14);
        } else {
          stmt.bindString(14, value.getFX_Response_Data());
        }
        if (value.getFX_Response_Date_Time() == null) {
          stmt.bindNull(15);
        } else {
          stmt.bindString(15, value.getFX_Response_Date_Time());
        }
      }
    };
    this.__preparedStmtOfDeleteAll = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "DELETE FROM fcm_pms_info";
        return _query;
      }
    };
  }

  @Override
  public void insert(final FCMPmsInfo pmsdata) {
    __db.assertNotSuspendingTransaction();
    __db.beginTransaction();
    try {
      __insertionAdapterOfFCMPmsInfo.insert(pmsdata);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void deleteAll() {
    __db.assertNotSuspendingTransaction();
    final SupportSQLiteStatement _stmt = __preparedStmtOfDeleteAll.acquire();
    __db.beginTransaction();
    try {
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfDeleteAll.release(_stmt);
    }
  }

  @Override
  public FCMPmsInfo getPmsInfoFromDB() {
    final String _sql = "SELECT * from fcm_pms_info";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
    try {
      final int _cursorIndexOfDate = CursorUtil.getColumnIndexOrThrow(_cursor, "Date");
      final int _cursorIndexOfEchoToken = CursorUtil.getColumnIndexOrThrow(_cursor, "Echo_Token");
      final int _cursorIndexOfNotificationType = CursorUtil.getColumnIndexOrThrow(_cursor, "Notification_Type");
      final int _cursorIndexOfNotificationData = CursorUtil.getColumnIndexOrThrow(_cursor, "Notification_Data");
      final int _cursorIndexOfNotificationDateTime = CursorUtil.getColumnIndexOrThrow(_cursor, "Notification_Date_Time");
      final int _cursorIndexOfPmsRequest = CursorUtil.getColumnIndexOrThrow(_cursor, "Pms_Request");
      final int _cursorIndexOfPmsRequestData = CursorUtil.getColumnIndexOrThrow(_cursor, "Pms_Request_Data");
      final int _cursorIndexOfPmsRequestDateTime = CursorUtil.getColumnIndexOrThrow(_cursor, "Pms_Request_Date_Time");
      final int _cursorIndexOfPmsResponseData = CursorUtil.getColumnIndexOrThrow(_cursor, "Pms_Response_Data");
      final int _cursorIndexOfPmsResponseDateTime = CursorUtil.getColumnIndexOrThrow(_cursor, "Pms_Response_Date_Time");
      final int _cursorIndexOfFXRequest = CursorUtil.getColumnIndexOrThrow(_cursor, "FX_Request");
      final int _cursorIndexOfFXRequestData = CursorUtil.getColumnIndexOrThrow(_cursor, "FX_Request_Data");
      final int _cursorIndexOfFXRequestDateTime = CursorUtil.getColumnIndexOrThrow(_cursor, "FX_Request_Date_Time");
      final int _cursorIndexOfFXResponseData = CursorUtil.getColumnIndexOrThrow(_cursor, "FX_Response_Data");
      final int _cursorIndexOfFXResponseDateTime = CursorUtil.getColumnIndexOrThrow(_cursor, "FX_Response_Date_Time");
      final FCMPmsInfo _result;
      if(_cursor.moveToFirst()) {
        _result = new FCMPmsInfo();
        final String _tmpDate;
        _tmpDate = _cursor.getString(_cursorIndexOfDate);
        _result.setDate(_tmpDate);
        final String _tmpEcho_Token;
        _tmpEcho_Token = _cursor.getString(_cursorIndexOfEchoToken);
        _result.setEcho_Token(_tmpEcho_Token);
        final String _tmpNotification_Type;
        _tmpNotification_Type = _cursor.getString(_cursorIndexOfNotificationType);
        _result.setNotification_Type(_tmpNotification_Type);
        final String _tmpNotification_Data;
        _tmpNotification_Data = _cursor.getString(_cursorIndexOfNotificationData);
        _result.setNotification_Data(_tmpNotification_Data);
        final String _tmpNotification_Date_Time;
        _tmpNotification_Date_Time = _cursor.getString(_cursorIndexOfNotificationDateTime);
        _result.setNotification_Date_Time(_tmpNotification_Date_Time);
        final String _tmpPms_Request;
        _tmpPms_Request = _cursor.getString(_cursorIndexOfPmsRequest);
        _result.setPms_Request(_tmpPms_Request);
        final String _tmpPms_Request_Data;
        _tmpPms_Request_Data = _cursor.getString(_cursorIndexOfPmsRequestData);
        _result.setPms_Request_Data(_tmpPms_Request_Data);
        final String _tmpPms_Request_Date_Time;
        _tmpPms_Request_Date_Time = _cursor.getString(_cursorIndexOfPmsRequestDateTime);
        _result.setPms_Request_Date_Time(_tmpPms_Request_Date_Time);
        final String _tmpPms_Response_Data;
        _tmpPms_Response_Data = _cursor.getString(_cursorIndexOfPmsResponseData);
        _result.setPms_Response_Data(_tmpPms_Response_Data);
        final String _tmpPms_Response_Date_Time;
        _tmpPms_Response_Date_Time = _cursor.getString(_cursorIndexOfPmsResponseDateTime);
        _result.setPms_Response_Date_Time(_tmpPms_Response_Date_Time);
        final String _tmpFX_Request;
        _tmpFX_Request = _cursor.getString(_cursorIndexOfFXRequest);
        _result.setFX_Request(_tmpFX_Request);
        final String _tmpFX_Request_Data;
        _tmpFX_Request_Data = _cursor.getString(_cursorIndexOfFXRequestData);
        _result.setFX_Request_Data(_tmpFX_Request_Data);
        final String _tmpFX_Request_Date_Time;
        _tmpFX_Request_Date_Time = _cursor.getString(_cursorIndexOfFXRequestDateTime);
        _result.setFX_Request_Date_Time(_tmpFX_Request_Date_Time);
        final String _tmpFX_Response_Data;
        _tmpFX_Response_Data = _cursor.getString(_cursorIndexOfFXResponseData);
        _result.setFX_Response_Data(_tmpFX_Response_Data);
        final String _tmpFX_Response_Date_Time;
        _tmpFX_Response_Date_Time = _cursor.getString(_cursorIndexOfFXResponseDateTime);
        _result.setFX_Response_Date_Time(_tmpFX_Response_Date_Time);
      } else {
        _result = null;
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public FCMPmsInfo getPmsInfoFromDBforDate(final String date) {
    final String _sql = "SELECT * from fcm_pms_info WHERE Date=?";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    if (date == null) {
      _statement.bindNull(_argIndex);
    } else {
      _statement.bindString(_argIndex, date);
    }
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
    try {
      final int _cursorIndexOfDate = CursorUtil.getColumnIndexOrThrow(_cursor, "Date");
      final int _cursorIndexOfEchoToken = CursorUtil.getColumnIndexOrThrow(_cursor, "Echo_Token");
      final int _cursorIndexOfNotificationType = CursorUtil.getColumnIndexOrThrow(_cursor, "Notification_Type");
      final int _cursorIndexOfNotificationData = CursorUtil.getColumnIndexOrThrow(_cursor, "Notification_Data");
      final int _cursorIndexOfNotificationDateTime = CursorUtil.getColumnIndexOrThrow(_cursor, "Notification_Date_Time");
      final int _cursorIndexOfPmsRequest = CursorUtil.getColumnIndexOrThrow(_cursor, "Pms_Request");
      final int _cursorIndexOfPmsRequestData = CursorUtil.getColumnIndexOrThrow(_cursor, "Pms_Request_Data");
      final int _cursorIndexOfPmsRequestDateTime = CursorUtil.getColumnIndexOrThrow(_cursor, "Pms_Request_Date_Time");
      final int _cursorIndexOfPmsResponseData = CursorUtil.getColumnIndexOrThrow(_cursor, "Pms_Response_Data");
      final int _cursorIndexOfPmsResponseDateTime = CursorUtil.getColumnIndexOrThrow(_cursor, "Pms_Response_Date_Time");
      final int _cursorIndexOfFXRequest = CursorUtil.getColumnIndexOrThrow(_cursor, "FX_Request");
      final int _cursorIndexOfFXRequestData = CursorUtil.getColumnIndexOrThrow(_cursor, "FX_Request_Data");
      final int _cursorIndexOfFXRequestDateTime = CursorUtil.getColumnIndexOrThrow(_cursor, "FX_Request_Date_Time");
      final int _cursorIndexOfFXResponseData = CursorUtil.getColumnIndexOrThrow(_cursor, "FX_Response_Data");
      final int _cursorIndexOfFXResponseDateTime = CursorUtil.getColumnIndexOrThrow(_cursor, "FX_Response_Date_Time");
      final FCMPmsInfo _result;
      if(_cursor.moveToFirst()) {
        _result = new FCMPmsInfo();
        final String _tmpDate;
        _tmpDate = _cursor.getString(_cursorIndexOfDate);
        _result.setDate(_tmpDate);
        final String _tmpEcho_Token;
        _tmpEcho_Token = _cursor.getString(_cursorIndexOfEchoToken);
        _result.setEcho_Token(_tmpEcho_Token);
        final String _tmpNotification_Type;
        _tmpNotification_Type = _cursor.getString(_cursorIndexOfNotificationType);
        _result.setNotification_Type(_tmpNotification_Type);
        final String _tmpNotification_Data;
        _tmpNotification_Data = _cursor.getString(_cursorIndexOfNotificationData);
        _result.setNotification_Data(_tmpNotification_Data);
        final String _tmpNotification_Date_Time;
        _tmpNotification_Date_Time = _cursor.getString(_cursorIndexOfNotificationDateTime);
        _result.setNotification_Date_Time(_tmpNotification_Date_Time);
        final String _tmpPms_Request;
        _tmpPms_Request = _cursor.getString(_cursorIndexOfPmsRequest);
        _result.setPms_Request(_tmpPms_Request);
        final String _tmpPms_Request_Data;
        _tmpPms_Request_Data = _cursor.getString(_cursorIndexOfPmsRequestData);
        _result.setPms_Request_Data(_tmpPms_Request_Data);
        final String _tmpPms_Request_Date_Time;
        _tmpPms_Request_Date_Time = _cursor.getString(_cursorIndexOfPmsRequestDateTime);
        _result.setPms_Request_Date_Time(_tmpPms_Request_Date_Time);
        final String _tmpPms_Response_Data;
        _tmpPms_Response_Data = _cursor.getString(_cursorIndexOfPmsResponseData);
        _result.setPms_Response_Data(_tmpPms_Response_Data);
        final String _tmpPms_Response_Date_Time;
        _tmpPms_Response_Date_Time = _cursor.getString(_cursorIndexOfPmsResponseDateTime);
        _result.setPms_Response_Date_Time(_tmpPms_Response_Date_Time);
        final String _tmpFX_Request;
        _tmpFX_Request = _cursor.getString(_cursorIndexOfFXRequest);
        _result.setFX_Request(_tmpFX_Request);
        final String _tmpFX_Request_Data;
        _tmpFX_Request_Data = _cursor.getString(_cursorIndexOfFXRequestData);
        _result.setFX_Request_Data(_tmpFX_Request_Data);
        final String _tmpFX_Request_Date_Time;
        _tmpFX_Request_Date_Time = _cursor.getString(_cursorIndexOfFXRequestDateTime);
        _result.setFX_Request_Date_Time(_tmpFX_Request_Date_Time);
        final String _tmpFX_Response_Data;
        _tmpFX_Response_Data = _cursor.getString(_cursorIndexOfFXResponseData);
        _result.setFX_Response_Data(_tmpFX_Response_Data);
        final String _tmpFX_Response_Date_Time;
        _tmpFX_Response_Date_Time = _cursor.getString(_cursorIndexOfFXResponseDateTime);
        _result.setFX_Response_Date_Time(_tmpFX_Response_Date_Time);
      } else {
        _result = null;
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public FCMPmsInfo FindFCmId(final String param) {
    final String _sql = "SELECT * FROM fcm_pms_info WHERE Echo_Token = ?";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    if (param == null) {
      _statement.bindNull(_argIndex);
    } else {
      _statement.bindString(_argIndex, param);
    }
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
    try {
      final int _cursorIndexOfDate = CursorUtil.getColumnIndexOrThrow(_cursor, "Date");
      final int _cursorIndexOfEchoToken = CursorUtil.getColumnIndexOrThrow(_cursor, "Echo_Token");
      final int _cursorIndexOfNotificationType = CursorUtil.getColumnIndexOrThrow(_cursor, "Notification_Type");
      final int _cursorIndexOfNotificationData = CursorUtil.getColumnIndexOrThrow(_cursor, "Notification_Data");
      final int _cursorIndexOfNotificationDateTime = CursorUtil.getColumnIndexOrThrow(_cursor, "Notification_Date_Time");
      final int _cursorIndexOfPmsRequest = CursorUtil.getColumnIndexOrThrow(_cursor, "Pms_Request");
      final int _cursorIndexOfPmsRequestData = CursorUtil.getColumnIndexOrThrow(_cursor, "Pms_Request_Data");
      final int _cursorIndexOfPmsRequestDateTime = CursorUtil.getColumnIndexOrThrow(_cursor, "Pms_Request_Date_Time");
      final int _cursorIndexOfPmsResponseData = CursorUtil.getColumnIndexOrThrow(_cursor, "Pms_Response_Data");
      final int _cursorIndexOfPmsResponseDateTime = CursorUtil.getColumnIndexOrThrow(_cursor, "Pms_Response_Date_Time");
      final int _cursorIndexOfFXRequest = CursorUtil.getColumnIndexOrThrow(_cursor, "FX_Request");
      final int _cursorIndexOfFXRequestData = CursorUtil.getColumnIndexOrThrow(_cursor, "FX_Request_Data");
      final int _cursorIndexOfFXRequestDateTime = CursorUtil.getColumnIndexOrThrow(_cursor, "FX_Request_Date_Time");
      final int _cursorIndexOfFXResponseData = CursorUtil.getColumnIndexOrThrow(_cursor, "FX_Response_Data");
      final int _cursorIndexOfFXResponseDateTime = CursorUtil.getColumnIndexOrThrow(_cursor, "FX_Response_Date_Time");
      final FCMPmsInfo _result;
      if(_cursor.moveToFirst()) {
        _result = new FCMPmsInfo();
        final String _tmpDate;
        _tmpDate = _cursor.getString(_cursorIndexOfDate);
        _result.setDate(_tmpDate);
        final String _tmpEcho_Token;
        _tmpEcho_Token = _cursor.getString(_cursorIndexOfEchoToken);
        _result.setEcho_Token(_tmpEcho_Token);
        final String _tmpNotification_Type;
        _tmpNotification_Type = _cursor.getString(_cursorIndexOfNotificationType);
        _result.setNotification_Type(_tmpNotification_Type);
        final String _tmpNotification_Data;
        _tmpNotification_Data = _cursor.getString(_cursorIndexOfNotificationData);
        _result.setNotification_Data(_tmpNotification_Data);
        final String _tmpNotification_Date_Time;
        _tmpNotification_Date_Time = _cursor.getString(_cursorIndexOfNotificationDateTime);
        _result.setNotification_Date_Time(_tmpNotification_Date_Time);
        final String _tmpPms_Request;
        _tmpPms_Request = _cursor.getString(_cursorIndexOfPmsRequest);
        _result.setPms_Request(_tmpPms_Request);
        final String _tmpPms_Request_Data;
        _tmpPms_Request_Data = _cursor.getString(_cursorIndexOfPmsRequestData);
        _result.setPms_Request_Data(_tmpPms_Request_Data);
        final String _tmpPms_Request_Date_Time;
        _tmpPms_Request_Date_Time = _cursor.getString(_cursorIndexOfPmsRequestDateTime);
        _result.setPms_Request_Date_Time(_tmpPms_Request_Date_Time);
        final String _tmpPms_Response_Data;
        _tmpPms_Response_Data = _cursor.getString(_cursorIndexOfPmsResponseData);
        _result.setPms_Response_Data(_tmpPms_Response_Data);
        final String _tmpPms_Response_Date_Time;
        _tmpPms_Response_Date_Time = _cursor.getString(_cursorIndexOfPmsResponseDateTime);
        _result.setPms_Response_Date_Time(_tmpPms_Response_Date_Time);
        final String _tmpFX_Request;
        _tmpFX_Request = _cursor.getString(_cursorIndexOfFXRequest);
        _result.setFX_Request(_tmpFX_Request);
        final String _tmpFX_Request_Data;
        _tmpFX_Request_Data = _cursor.getString(_cursorIndexOfFXRequestData);
        _result.setFX_Request_Data(_tmpFX_Request_Data);
        final String _tmpFX_Request_Date_Time;
        _tmpFX_Request_Date_Time = _cursor.getString(_cursorIndexOfFXRequestDateTime);
        _result.setFX_Request_Date_Time(_tmpFX_Request_Date_Time);
        final String _tmpFX_Response_Data;
        _tmpFX_Response_Data = _cursor.getString(_cursorIndexOfFXResponseData);
        _result.setFX_Response_Data(_tmpFX_Response_Data);
        final String _tmpFX_Response_Date_Time;
        _tmpFX_Response_Date_Time = _cursor.getString(_cursorIndexOfFXResponseDateTime);
        _result.setFX_Response_Date_Time(_tmpFX_Response_Date_Time);
      } else {
        _result = null;
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }
}
