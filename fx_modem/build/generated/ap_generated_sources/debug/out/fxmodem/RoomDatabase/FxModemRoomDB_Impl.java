package fxmodem.RoomDatabase;

import androidx.room.DatabaseConfiguration;
import androidx.room.InvalidationTracker;
import androidx.room.RoomOpenHelper;
import androidx.room.RoomOpenHelper.Delegate;
import androidx.room.RoomOpenHelper.ValidationResult;
import androidx.room.util.DBUtil;
import androidx.room.util.TableInfo;
import androidx.room.util.TableInfo.Column;
import androidx.room.util.TableInfo.ForeignKey;
import androidx.room.util.TableInfo.Index;
import androidx.sqlite.db.SupportSQLiteDatabase;
import androidx.sqlite.db.SupportSQLiteOpenHelper;
import androidx.sqlite.db.SupportSQLiteOpenHelper.Callback;
import androidx.sqlite.db.SupportSQLiteOpenHelper.Configuration;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

@SuppressWarnings({"unchecked", "deprecation"})
public final class FxModemRoomDB_Impl extends FxModemRoomDB {
  private volatile FCMPmsInfoDAO _fCMPmsInfoDAO;

  @Override
  protected SupportSQLiteOpenHelper createOpenHelper(DatabaseConfiguration configuration) {
    final SupportSQLiteOpenHelper.Callback _openCallback = new RoomOpenHelper(configuration, new RoomOpenHelper.Delegate(1) {
      @Override
      public void createAllTables(SupportSQLiteDatabase _db) {
        _db.execSQL("CREATE TABLE IF NOT EXISTS `fcm_pms_info` (`Date` TEXT NOT NULL, `Echo_Token` TEXT, `Notification_Type` TEXT, `Notification_Data` TEXT, `Notification_Date_Time` TEXT, `Pms_Request` TEXT, `Pms_Request_Data` TEXT, `Pms_Request_Date_Time` TEXT, `Pms_Response_Data` TEXT, `Pms_Response_Date_Time` TEXT, `FX_Request` TEXT, `FX_Request_Data` TEXT, `FX_Request_Date_Time` TEXT, `FX_Response_Data` TEXT, `FX_Response_Date_Time` TEXT, PRIMARY KEY(`Date`))");
        _db.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
        _db.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, 'ef9cb8a750b8aab2730d930e2191f40a')");
      }

      @Override
      public void dropAllTables(SupportSQLiteDatabase _db) {
        _db.execSQL("DROP TABLE IF EXISTS `fcm_pms_info`");
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onDestructiveMigration(_db);
          }
        }
      }

      @Override
      protected void onCreate(SupportSQLiteDatabase _db) {
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onCreate(_db);
          }
        }
      }

      @Override
      public void onOpen(SupportSQLiteDatabase _db) {
        mDatabase = _db;
        internalInitInvalidationTracker(_db);
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onOpen(_db);
          }
        }
      }

      @Override
      public void onPreMigrate(SupportSQLiteDatabase _db) {
        DBUtil.dropFtsSyncTriggers(_db);
      }

      @Override
      public void onPostMigrate(SupportSQLiteDatabase _db) {
      }

      @Override
      protected RoomOpenHelper.ValidationResult onValidateSchema(SupportSQLiteDatabase _db) {
        final HashMap<String, TableInfo.Column> _columnsFcmPmsInfo = new HashMap<String, TableInfo.Column>(15);
        _columnsFcmPmsInfo.put("Date", new TableInfo.Column("Date", "TEXT", true, 1, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsFcmPmsInfo.put("Echo_Token", new TableInfo.Column("Echo_Token", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsFcmPmsInfo.put("Notification_Type", new TableInfo.Column("Notification_Type", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsFcmPmsInfo.put("Notification_Data", new TableInfo.Column("Notification_Data", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsFcmPmsInfo.put("Notification_Date_Time", new TableInfo.Column("Notification_Date_Time", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsFcmPmsInfo.put("Pms_Request", new TableInfo.Column("Pms_Request", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsFcmPmsInfo.put("Pms_Request_Data", new TableInfo.Column("Pms_Request_Data", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsFcmPmsInfo.put("Pms_Request_Date_Time", new TableInfo.Column("Pms_Request_Date_Time", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsFcmPmsInfo.put("Pms_Response_Data", new TableInfo.Column("Pms_Response_Data", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsFcmPmsInfo.put("Pms_Response_Date_Time", new TableInfo.Column("Pms_Response_Date_Time", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsFcmPmsInfo.put("FX_Request", new TableInfo.Column("FX_Request", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsFcmPmsInfo.put("FX_Request_Data", new TableInfo.Column("FX_Request_Data", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsFcmPmsInfo.put("FX_Request_Date_Time", new TableInfo.Column("FX_Request_Date_Time", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsFcmPmsInfo.put("FX_Response_Data", new TableInfo.Column("FX_Response_Data", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsFcmPmsInfo.put("FX_Response_Date_Time", new TableInfo.Column("FX_Response_Date_Time", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        final HashSet<TableInfo.ForeignKey> _foreignKeysFcmPmsInfo = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesFcmPmsInfo = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoFcmPmsInfo = new TableInfo("fcm_pms_info", _columnsFcmPmsInfo, _foreignKeysFcmPmsInfo, _indicesFcmPmsInfo);
        final TableInfo _existingFcmPmsInfo = TableInfo.read(_db, "fcm_pms_info");
        if (! _infoFcmPmsInfo.equals(_existingFcmPmsInfo)) {
          return new RoomOpenHelper.ValidationResult(false, "fcm_pms_info(fxmodem.Models.FCMPMSData.FCMPmsInfo).\n"
                  + " Expected:\n" + _infoFcmPmsInfo + "\n"
                  + " Found:\n" + _existingFcmPmsInfo);
        }
        return new RoomOpenHelper.ValidationResult(true, null);
      }
    }, "ef9cb8a750b8aab2730d930e2191f40a", "77c2abc5fa43f5eb1a83d501d0cb2c3f");
    final SupportSQLiteOpenHelper.Configuration _sqliteConfig = SupportSQLiteOpenHelper.Configuration.builder(configuration.context)
        .name(configuration.name)
        .callback(_openCallback)
        .build();
    final SupportSQLiteOpenHelper _helper = configuration.sqliteOpenHelperFactory.create(_sqliteConfig);
    return _helper;
  }

  @Override
  protected InvalidationTracker createInvalidationTracker() {
    final HashMap<String, String> _shadowTablesMap = new HashMap<String, String>(0);
    HashMap<String, Set<String>> _viewTables = new HashMap<String, Set<String>>(0);
    return new InvalidationTracker(this, _shadowTablesMap, _viewTables, "fcm_pms_info");
  }

  @Override
  public void clearAllTables() {
    super.assertNotMainThread();
    final SupportSQLiteDatabase _db = super.getOpenHelper().getWritableDatabase();
    try {
      super.beginTransaction();
      _db.execSQL("DELETE FROM `fcm_pms_info`");
      super.setTransactionSuccessful();
    } finally {
      super.endTransaction();
      _db.query("PRAGMA wal_checkpoint(FULL)").close();
      if (!_db.inTransaction()) {
        _db.execSQL("VACUUM");
      }
    }
  }

  @Override
  public FCMPmsInfoDAO wordDao() {
    if (_fCMPmsInfoDAO != null) {
      return _fCMPmsInfoDAO;
    } else {
      synchronized(this) {
        if(_fCMPmsInfoDAO == null) {
          _fCMPmsInfoDAO = new FCMPmsInfoDAO_Impl(this);
        }
        return _fCMPmsInfoDAO;
      }
    }
  }
}
