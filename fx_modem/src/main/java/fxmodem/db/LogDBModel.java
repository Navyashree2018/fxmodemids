package fxmodem.db;

public class LogDBModel {
    public String FXApiFirstLog = "";
    public String FXRequestFirstLog = "";
    public String FXResponseFirstLog = "";
    public String PmsApiLog = "";
    public String PmsRequestLog = "";
    public String PmsResponseLog = "";
    public String FXApiLastLog = "";
    public String FXRequestLastLog = "";
    public String FXResponseLastLog = "";

    LogDBModel(){}

    public LogDBModel(String FXApiFirstLog, String FXRequestFirstLog, String FXResponseFirstLog, String pmsApiLog, String pmsRequestLog, String pmsResponseLog, String FXApiLastLog, String FXRequestLastLog, String FXResponseLastLog) {
        this.FXApiFirstLog = FXApiFirstLog;
        this.FXRequestFirstLog = FXRequestFirstLog;
        this.FXResponseFirstLog = FXResponseFirstLog;
        PmsApiLog = pmsApiLog;
        PmsRequestLog = pmsRequestLog;
        PmsResponseLog = pmsResponseLog;
        this.FXApiLastLog = FXApiLastLog;
        this.FXRequestLastLog = FXRequestLastLog;
        this.FXResponseLastLog = FXResponseLastLog;
    }

    public String getFXApiFirstLog() {
        return FXApiFirstLog;
    }

    public void setFXApiFirstLog(String FXApiFirstLog) {
        this.FXApiFirstLog = FXApiFirstLog;
    }

    public String getFXRequestFirstLog() {
        return FXRequestFirstLog;
    }

    public void setFXRequestFirstLog(String FXRequestFirstLog) {
        this.FXRequestFirstLog = FXRequestFirstLog;
    }

    public String getFXResponseFirstLog() {
        return FXResponseFirstLog;
    }

    public void setFXResponseFirstLog(String FXResponseFirstLog) {
        this.FXResponseFirstLog = FXResponseFirstLog;
    }

    public String getPmsApiLog() {
        return PmsApiLog;
    }

    public void setPmsApiLog(String pmsApiLog) {
        PmsApiLog = pmsApiLog;
    }

    public String getPmsRequestLog() {
        return PmsRequestLog;
    }

    public void setPmsRequestLog(String pmsRequestLog) {
        PmsRequestLog = pmsRequestLog;
    }

    public String getPmsResponseLog() {
        return PmsResponseLog;
    }

    public void setPmsResponseLog(String pmsResponseLog) {
        PmsResponseLog = pmsResponseLog;
    }

    public String getFXApiLastLog() {
        return FXApiLastLog;
    }

    public void setFXApiLastLog(String FXApiLastLog) {
        this.FXApiLastLog = FXApiLastLog;
    }

    public String getFXRequestLastLog() {
        return FXRequestLastLog;
    }

    public void setFXRequestLastLog(String FXRequestLastLog) {
        this.FXRequestLastLog = FXRequestLastLog;
    }

    public String getFXResponseLastLog() {
        return FXResponseLastLog;
    }

    public void setFXResponseLastLog(String FXResponseLastLog) {
        this.FXResponseLastLog = FXResponseLastLog;
    }

    @Override
    public String toString() {
        return "LogDBModel{" +
               /* "FXApiFirstLog='" + FXApiFirstLog + '\'' +
                ", FXRequestFirstLog='" + FXRequestFirstLog + '\'' +
                ", FXResponseFirstLog='" + FXResponseFirstLog + '\'' +*/
                ", PmsApiLog='" + PmsApiLog + '\'' +
                ", PmsRequestLog='" + PmsRequestLog + '\'' +
                ", PmsResponseLog='" + PmsResponseLog + '\'' +
                ", FXApiLastLog='" + FXApiLastLog + '\'' +
                ", FXRequestLastLog='" + FXRequestLastLog + '\'' +
                ", FXResponseLastLog='" + FXResponseLastLog + '\'' +
                '}';
    }
}
