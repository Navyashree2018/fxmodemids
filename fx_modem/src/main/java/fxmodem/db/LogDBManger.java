package fxmodem.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;


public class LogDBManger extends SQLiteOpenHelper {
    private static final int DB_VERSION = 1;
    public static final String DB_DATABASE_NAME = "FX_MODEM_DB";
    private static final String DB_TABLE_NAME = "FX_MODEM_TABLE";

    private static final String KEY_SERIAL_NO = "SerialNo";
    private static final String KEY_DATE = "Date";
    //private static final String KEY_FX_Notification_Message = "FXNotificationMessageData";
    private static final String KEY_FX_ApiFirstLog = "FXApiFirstLog";
    private static final String KEY_FX_RequestFirstLog = "FXRequestFirstLog";
    private static final String KEY_FX_ResponseFirstLog = "FXResponseFirstLog";
    private static final String KEY_PMS_ApiLog = "PmsApiLog";
    private static final String KEY_PMS_RequestLog = "PmsRequestLog";
    private static final String KEY_PMS_ResponseLog = "PmsResponseLog";
    private static final String KEY_FX_ApiLastLog = "FXApiLastLog";
    private static final String KEY_FX_RequestLastLog = "FXRequestLastLog";
    private static final String KEY_FX_ResponseLastLog = "FXResponseLastLog";

    public LogDBManger(Context context) {
        super(context, DB_DATABASE_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String CREATE_FCM_TABLE = "CREATE TABLE " + DB_TABLE_NAME + "("
                + KEY_SERIAL_NO + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + KEY_DATE + " TEXT,"
                + KEY_FX_ApiFirstLog + " TEXT,"
                + KEY_FX_RequestFirstLog + " TEXT,"
                + KEY_FX_ResponseFirstLog + " TEXT,"
                + KEY_PMS_ApiLog + " TEXT,"
                + KEY_PMS_RequestLog + " TEXT,"
                + KEY_PMS_ResponseLog + " TEXT,"
                + KEY_FX_ApiLastLog + " TEXT,"
                + KEY_FX_RequestLastLog + " TEXT,"
                + KEY_FX_ResponseLastLog + " TEXT" + ")";
        sqLiteDatabase.execSQL(CREATE_FCM_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + DB_TABLE_NAME);
        onCreate(sqLiteDatabase);
    }

    public void insertLogDBData(String Date, String FX_ApiFirstLog, String FX_RequestFirstLog, String FX_ResponseFirstLog,
                                String PMS_ApiLog, String PMS_RequestLog, String PMS_ResponseLog,
                                String FX_ApiLastLog, String FX_RequestLastLog, String FX_ResponseLastLog) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_DATE, Date);
        contentValues.put(KEY_FX_ApiFirstLog, FX_ApiFirstLog);
        contentValues.put(KEY_FX_RequestFirstLog, FX_RequestFirstLog);
        contentValues.put(KEY_FX_ResponseFirstLog, FX_ResponseFirstLog);
        contentValues.put(KEY_PMS_ApiLog, PMS_ApiLog);
        contentValues.put(KEY_PMS_RequestLog, PMS_RequestLog);
        contentValues.put(KEY_PMS_ResponseLog, PMS_ResponseLog);
        contentValues.put(KEY_FX_ApiLastLog, FX_ApiLastLog);
        contentValues.put(KEY_FX_RequestLastLog, FX_RequestLastLog);
        contentValues.put(KEY_FX_ResponseLastLog, FX_ResponseLastLog);
        db.insert(DB_TABLE_NAME, null, contentValues);
    }

    public void deleteLogTableData() {
        SQLiteDatabase db = this.getWritableDatabase();
        //db.execSQL("delete from " + DB_TABLE_NAME);


        Cursor cursor = db.rawQuery("Select * from FX_MODEM_TABLE", null);
        if (cursor.getCount() > 10) {

            // String selectQuery = "SELECT  * FROM " + DB_TABLE_NAME +" WHERE " +KEY_SERIAL_NO+ " IN (SELECT "+KEY_SERIAL_NO + " FROM "+DB_TABLE_NAME+" WHERE " +KEY_SERIAL_NO+ " ORDER BY Col DESC LIMIT 10)";

            //working
            String selectQuery = "delete from " + DB_TABLE_NAME + " WHERE " + KEY_SERIAL_NO + " NOT IN (SELECT " + KEY_SERIAL_NO + " FROM " + DB_TABLE_NAME + " WHERE " + KEY_SERIAL_NO + " ORDER BY  " + KEY_SERIAL_NO + " DESC LIMIT 30)";

            db.execSQL(selectQuery);

            Log.i("LogsList", "deleted for >30" + selectQuery + "\ndelete method Count" + cursor.getCount());

        }
    }

/*
    public LogDBModel getLog() {

            SQLiteDatabase db = this.getReadableDatabase();
            LogDBModel logDBModel = new LogDBModel();
            try {
                Cursor cursor = db.rawQuery("Select * from FX_MODEM_TABLE", null);
                if (cursor != null && cursor.getCount()>0) {
                    cursor.moveToFirst();

                    logDBModel = new LogDBModel(cursor.getString(2), cursor.getString(3), cursor.getString(4), cursor.getString(5),
                            cursor.getString(6), cursor.getString(7), cursor.getString(8), cursor.getString(9), cursor.getString(10));
                }
            }
            catch (Exception e){}
            return logDBModel;

    }
*/


    public ArrayList<LogDBModel> getLog() {


        SQLiteDatabase db = this.getReadableDatabase();
        deleteLogTableData();

        ArrayList<LogDBModel> logDBModellist = new ArrayList<>();
        try {
            Cursor cursor = db.rawQuery("Select * from FX_MODEM_TABLE", null);
            Log.i("LogsList", " ====" + "Count" + cursor.getCount());


            if (cursor != null && cursor.getCount() > 0) {

                if (cursor.moveToFirst()) {
                    while (!cursor.isAfterLast()) {
                        LogDBModel logDBModel = new LogDBModel();

                        logDBModel = new LogDBModel(cursor.getString(2), cursor.getString(3), cursor.getString(4), cursor.getString(5),
                                cursor.getString(6), cursor.getString(7), cursor.getString(8), cursor.getString(9), cursor.getString(10));

                        logDBModellist.add(logDBModel);
                        cursor.moveToNext();
                    }
                }
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.i("LogsList", " ====" + logDBModellist.toString());

        return logDBModellist;

    }
}
