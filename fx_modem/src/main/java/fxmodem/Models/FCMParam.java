package fxmodem.Models;

public class FCMParam {


    private String Source;

    private String LoginID;

    private String DeviceID;

    private String PMSCustCode;

    private String APPVersion;

    private String PlatformVersion;

    private String IsOnline;

    private String Platform;

    public String getSource ()
    {
        return Source;
    }

    public void setSource (String Source)
    {
        this.Source = Source;
    }

    public String getLoginID ()
    {
        return LoginID;
    }

    public void setLoginID (String LoginID)
    {
        this.LoginID = LoginID;
    }

    public String getDeviceID ()
    {
        return DeviceID;
    }

    public void setDeviceID (String DeviceID)
    {
        this.DeviceID = DeviceID;
    }

    public String getPMSCustCode ()
    {
        return PMSCustCode;
    }

    public void setPMSCustCode (String PMSCustCode)
    {
        this.PMSCustCode = PMSCustCode;
    }

    public String getAPPVersion ()
    {
        return APPVersion;
    }

    public void setAPPVersion (String APPVersion)
    {
        this.APPVersion = APPVersion;
    }

    public String getPlatformVersion ()
    {
        return PlatformVersion;
    }

    public void setPlatformVersion (String PlatformVersion)
    {
        this.PlatformVersion = PlatformVersion;
    }

    public String getIsOnline ()
    {
        return IsOnline;
    }

    public void setIsOnline (String IsOnline)
    {
        this.IsOnline = IsOnline;
    }

    public String getPlatform ()
    {
        return Platform;
    }

    public void setPlatform (String Platform)
    {
        this.Platform = Platform;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Source = "+Source+", LoginID = "+LoginID+", DeviceID = "+DeviceID+", PMSCustCode = "+PMSCustCode+", APPVersion = "+APPVersion+", PlatformVersion = "+PlatformVersion+", IsOnline = "+IsOnline+", Platform = "+Platform+"]";
    }
}
