package fxmodem.Models.FCMPMSData;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "fcm_pms_info")
public class FCMPmsInfo {


    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "Date")
    String Date;


    @ColumnInfo(name = "Echo_Token")
    String Echo_Token;

    @ColumnInfo(name = "Notification_Type")
    String Notification_Type;

    @ColumnInfo(name = "Notification_Data")
    String Notification_Data;

    @ColumnInfo(name = "Notification_Date_Time")
    String Notification_Date_Time;

    @ColumnInfo(name = "Pms_Request")
    String Pms_Request;

    @ColumnInfo(name = "Pms_Request_Data")
    String Pms_Request_Data;

    @ColumnInfo(name = "Pms_Request_Date_Time")
    String Pms_Request_Date_Time;

    @ColumnInfo(name = "Pms_Response_Data")
    String Pms_Response_Data;

    @ColumnInfo(name = "Pms_Response_Date_Time")
    String Pms_Response_Date_Time;

    @ColumnInfo(name = "FX_Request")
    String FX_Request;

    @ColumnInfo(name = "FX_Request_Data")
    String FX_Request_Data;

    @ColumnInfo(name = "FX_Request_Date_Time")
    String FX_Request_Date_Time;

    @ColumnInfo(name = "FX_Response_Data")
    String FX_Response_Data;

    @ColumnInfo(name = "FX_Response_Date_Time")
    String FX_Response_Date_Time;

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getEcho_Token() {
        return Echo_Token;
    }

    public void setEcho_Token(String echo_Token) {
        Echo_Token = echo_Token;
    }

    public String getNotification_Type() {
        return Notification_Type;
    }

    public void setNotification_Type(String notification_Type) {
        Notification_Type = notification_Type;
    }

    public String getNotification_Data() {
        return Notification_Data;
    }

    public void setNotification_Data(String notification_Data) {
        Notification_Data = notification_Data;
    }

    public String getNotification_Date_Time() {
        return Notification_Date_Time;
    }

    public void setNotification_Date_Time(String notification_Date_Time) {
        Notification_Date_Time = notification_Date_Time;
    }

    public String getPms_Request() {
        return Pms_Request;
    }

    public void setPms_Request(String pms_Request) {
        Pms_Request = pms_Request;
    }

    public String getPms_Request_Data() {
        return Pms_Request_Data;
    }

    public void setPms_Request_Data(String pms_Request_Data) {
        Pms_Request_Data = pms_Request_Data;
    }

    public String getPms_Request_Date_Time() {
        return Pms_Request_Date_Time;
    }

    public void setPms_Request_Date_Time(String pms_Request_Date_Time) {
        Pms_Request_Date_Time = pms_Request_Date_Time;
    }

    public String getPms_Response_Data() {
        return Pms_Response_Data;
    }

    public void setPms_Response_Data(String pms_Response_Data) {
        Pms_Response_Data = pms_Response_Data;
    }

    public String getPms_Response_Date_Time() {
        return Pms_Response_Date_Time;
    }

    public void setPms_Response_Date_Time(String pms_Response_Date_Time) {
        Pms_Response_Date_Time = pms_Response_Date_Time;
    }

    public String getFX_Request() {
        return FX_Request;
    }

    public void setFX_Request(String FX_Request) {
        this.FX_Request = FX_Request;
    }

    public String getFX_Request_Data() {
        return FX_Request_Data;
    }

    public void setFX_Request_Data(String FX_Request_Data) {
        this.FX_Request_Data = FX_Request_Data;
    }

    public String getFX_Request_Date_Time() {
        return FX_Request_Date_Time;
    }

    public void setFX_Request_Date_Time(String FX_Request_Date_Time) {
        this.FX_Request_Date_Time = FX_Request_Date_Time;
    }

    public String getFX_Response_Data() {
        return FX_Response_Data;
    }

    public void setFX_Response_Data(String FX_Response_Data) {
        this.FX_Response_Data = FX_Response_Data;
    }

    public String getFX_Response_Date_Time() {
        return FX_Response_Date_Time;
    }

    public void setFX_Response_Date_Time(String FX_Response_Date_Time) {
        this.FX_Response_Date_Time = FX_Response_Date_Time;
    }

    @Override
    public String toString() {
        return "FCMPmsInfo{" +
                "Date='" + Date + '\'' +
                ", Echo_Token='" + Echo_Token + '\'' +
                ", Notification_Type='" + Notification_Type + '\'' +
                ", Notification_Data='" + Notification_Data + '\'' +
                ", Notification_Date_Time='" + Notification_Date_Time + '\'' +
                ", Pms_Request='" + Pms_Request + '\'' +
                ", Pms_Request_Data='" + Pms_Request_Data + '\'' +
                ", Pms_Request_Date_Time='" + Pms_Request_Date_Time + '\'' +
                ", Pms_Response_Data='" + Pms_Response_Data + '\'' +
                ", Pms_Response_Date_Time='" + Pms_Response_Date_Time + '\'' +
                ", FX_Request='" + FX_Request + '\'' +
                ", FX_Request_Data='" + FX_Request_Data + '\'' +
                ", FX_Request_Date_Time='" + FX_Request_Date_Time + '\'' +
                ", FX_Response_Data='" + FX_Response_Data + '\'' +
                ", FX_Response_Date_Time='" + FX_Response_Date_Time + '\'' +
                '}';
    }


}
