package fxmodem.Models.FCMPMSData;


public class PMSDataInterface {


    private String PMSCustCode;

    private String RequestID;

    private String ResponseStatus;

    private String ResponseData;

    private String ResponseRemarks;

    public String getPMSCustCode ()
    {
        return PMSCustCode;
    }

    public void setPMSCustCode (String PMSCustCode)
    {
        this.PMSCustCode = PMSCustCode;
    }

    public String getRequestID ()
    {
        return RequestID;
    }

    public void setRequestID (String RequestID)
    {
        this.RequestID = RequestID;
    }

    public String getResponseStatus ()
    {
        return ResponseStatus;
    }

    public void setResponseStatus (String ResponseStatus)
    {
        this.ResponseStatus = ResponseStatus;
    }

    public String getResponseData ()
    {
        return ResponseData;
    }

    public void setResponseData (String ResponseData)
    {
        this.ResponseData = ResponseData;
    }

    public String getResponseRemarks ()
    {
        return ResponseRemarks;
    }

    public void setResponseRemarks (String ResponseRemarks)
    {
        this.ResponseRemarks = ResponseRemarks;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [PMSCustCode = "+PMSCustCode+", RequestID = "+RequestID+", ResponseStatus = "+ResponseStatus+", ResponseData = "+ResponseData+", ResponseRemarks = "+ResponseRemarks+"]";
    }
}
