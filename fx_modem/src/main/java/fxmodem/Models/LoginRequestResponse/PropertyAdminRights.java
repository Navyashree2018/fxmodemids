package fxmodem.Models.LoginRequestResponse;

class PropertyAdminRights {

    private String PropertyGroupCode;

    private String PmsCustCode;

    public String getPropertyGroupCode ()
    {
        return PropertyGroupCode;
    }

    public void setPropertyGroupCode (String PropertyGroupCode)
    {
        this.PropertyGroupCode = PropertyGroupCode;
    }

    public String getPmsCustCode ()
    {
        return PmsCustCode;
    }

    public void setPmsCustCode (String PmsCustCode)
    {
        this.PmsCustCode = PmsCustCode;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [PropertyGroupCode = "+PropertyGroupCode+", PmsCustCode = "+PmsCustCode+"]";
    }
}
