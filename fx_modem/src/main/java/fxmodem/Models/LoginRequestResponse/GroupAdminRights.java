package fxmodem.Models.LoginRequestResponse;

public class GroupAdminRights {
    private String PropertyGroupCode;

    public String getPropertyGroupCode ()
    {
        return PropertyGroupCode;
    }

    public void setPropertyGroupCode (String PropertyGroupCode)
    {
        this.PropertyGroupCode = PropertyGroupCode;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [PropertyGroupCode = "+PropertyGroupCode+"]";
    }
}
