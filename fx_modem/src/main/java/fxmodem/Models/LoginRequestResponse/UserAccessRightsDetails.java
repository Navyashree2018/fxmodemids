package fxmodem.Models.LoginRequestResponse;

import java.util.ArrayList;

public class UserAccessRightsDetails {

    private ArrayList<PropertyAdminRights> PropertyAdminRights;

    private ArrayList<PropertySpecificrights> PropertySpecific;

    private ArrayList<GroupAdminRights> GroupAdminRights;

    private UserDetails UserDetails;

    private ArrayList<DefaultHotel> DefaultHotel;

    private ArrayList<SubscribedProducts> SubscribedProducts;

    private ArrayList<UserRights> UserRights;
    private String Access_Token;

    public String getAccess_Token() {
        return Access_Token;
    }

    public void setAccess_Token(String access_Token) {
        Access_Token = access_Token;
    }


    public ArrayList<PropertyAdminRights> getPropertyAdminRights() {
        return PropertyAdminRights;
    }

    public void setPropertyAdminRights(ArrayList<PropertyAdminRights> propertyAdminRights) {
        PropertyAdminRights = propertyAdminRights;
    }

    public ArrayList<PropertySpecificrights> getPropertySpecific() {
        return PropertySpecific;
    }

    public void setPropertySpecific(ArrayList<PropertySpecificrights> propertySpecific) {
        PropertySpecific = propertySpecific;
    }

    public ArrayList<GroupAdminRights> getGroupAdminRights() {
        return GroupAdminRights;
    }

    public void setGroupAdminRights(ArrayList<GroupAdminRights> groupAdminRights) {
        GroupAdminRights = groupAdminRights;
    }

    public UserDetails getUserDetails() {
        return UserDetails;
    }

    public void setUserDetails(UserDetails userDetails) {
        UserDetails = userDetails;
    }

    public ArrayList<DefaultHotel> getDefaultHotel() {
        return DefaultHotel;
    }

    public void setDefaultHotel(ArrayList<DefaultHotel> defaultHotel) {
        DefaultHotel = defaultHotel;
    }

    public ArrayList<SubscribedProducts> getSubscribedProducts() {
        return SubscribedProducts;
    }

    public void setSubscribedProducts(ArrayList<SubscribedProducts> subscribedProducts) {
        SubscribedProducts = subscribedProducts;
    }

    public ArrayList<UserRights> getUserRights() {
        return UserRights;
    }

    public void setUserRights(ArrayList<UserRights> userRights) {
        UserRights = userRights;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [SubscribedProducts = "+SubscribedProducts+", UserRights = "+UserRights+", PropertyAdminRights = "+PropertyAdminRights+",PropertySpecific = "+PropertySpecific+", GroupAdminRights = "+GroupAdminRights+", UserDetails = "+UserDetails+", DefaultHotel = "+DefaultHotel+"]";
    }

}
