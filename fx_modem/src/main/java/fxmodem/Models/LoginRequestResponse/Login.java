package fxmodem.Models.LoginRequestResponse;


public class Login {
    private String UserID;
    private String Password;
    private String ProductCode;
    private String DeviceId;
    private String DeviceIP;

    public String getUserID() {
        return UserID;
    }

    public void setUserID(String userID) {
        UserID = userID;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getProductCode() {
        return ProductCode;
    }

    public void setProductCode(String productCode) {
        ProductCode = productCode;
    }

    public String getDeviceId() {
        return DeviceId;
    }

    public void setDeviceId(String deviceId) {
        DeviceId = deviceId;
    }

    public String getDeviceIP() {
        return DeviceIP;
    }

    public void setDeviceIP(String deviceIP) {
        DeviceIP = deviceIP;
    }

    @Override
    public String toString() {
        return "ClassPojo [UserID = " + UserID + ", Password = " + Password + ", ProductCode = " + ProductCode + ", DeviceId = "+DeviceId+", DeviceIP = "+DeviceIP+"]";
    }
}



