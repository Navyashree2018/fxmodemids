package fxmodem.Models.LoginRequestResponse;

public class Roles {

    private String FunctionCode;

    private String AccessRights;

    private String FunctionName;

    private String HasAccess;

    public String getHasAccess() {
        return HasAccess;
    }

    public void setHasAccess(String hasAccess) {
        HasAccess = hasAccess;
    }


    public String getFunctionCode ()
    {
        return FunctionCode;
    }

    public void setFunctionCode (String FunctionCode)
    {
        this.FunctionCode = FunctionCode;
    }

    public String getAccessRights ()
    {
        return AccessRights;
    }

    public void setAccessRights (String AccessRights)
    {
        this.AccessRights = AccessRights;
    }

    public String getFunctionName ()
    {
        return FunctionName;
    }

    public void setFunctionName (String FunctionName)
    {
        this.FunctionName = FunctionName;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [FunctionCode = "+FunctionCode+", AccessRights = "+AccessRights+", FunctionName = "+FunctionName+"]";
    }

}
