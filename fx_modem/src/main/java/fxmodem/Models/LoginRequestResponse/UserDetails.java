package fxmodem.Models.LoginRequestResponse;

public class UserDetails {

    private String LoginID;

    private String UserType;

    private String ImageFileName;

    private String DefaultGroupID;

    private String Password;

    private String UserName;

    private String HoldingCompanyID;

    private String HoldingCompanyCode;

    private String ImageFilePath;

    private String DefaultPMSCUSTCODE;

    private String DefaultPMSCode;

    private String DateFormat;

    private String UnsubscribedProducts;

    private String PreferredLanguage;

    private String DefaultGroupCode;

    private String ImageFileType;

    private String UTCOffset;

    private String TimeFormat;
    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getLoginID ()
    {
        return LoginID;
    }

    public void setLoginID (String LoginID)
    {
        this.LoginID = LoginID;
    }

    public String getUserType ()
    {
        return UserType;
    }

    public void setUserType (String UserType)
    {
        this.UserType = UserType;
    }

    public String getImageFileName ()
    {
        return ImageFileName;
    }

    public void setImageFileName (String ImageFileName)
    {
        this.ImageFileName = ImageFileName;
    }

    public String getDefaultGroupID ()
    {
        return DefaultGroupID;
    }

    public void setDefaultGroupID (String DefaultGroupID)
    {
        this.DefaultGroupID = DefaultGroupID;
    }



    public String getUserName ()
    {
        return UserName;
    }

    public void setUserName (String UserName)
    {
        this.UserName = UserName;
    }

    public String getHoldingCompanyID ()
    {
        return HoldingCompanyID;
    }

    public void setHoldingCompanyID (String HoldingCompanyID)
    {
        this.HoldingCompanyID = HoldingCompanyID;
    }

    public String getHoldingCompanyCode ()
    {
        return HoldingCompanyCode;
    }

    public void setHoldingCompanyCode (String HoldingCompanyCode)
    {
        this.HoldingCompanyCode = HoldingCompanyCode;
    }

    public String getImageFilePath ()
    {
        return ImageFilePath;
    }

    public void setImageFilePath (String ImageFilePath)
    {
        this.ImageFilePath = ImageFilePath;
    }

    public String getDefaultPMSCUSTCODE ()
    {
        return DefaultPMSCUSTCODE;
    }

    public void setDefaultPMSCUSTCODE (String DefaultPMSCUSTCODE)
    {
        this.DefaultPMSCUSTCODE = DefaultPMSCUSTCODE;
    }

    public String getDefaultPMSCode ()
    {
        return DefaultPMSCode;
    }

    public void setDefaultPMSCode (String DefaultPMSCode)
    {
        this.DefaultPMSCode = DefaultPMSCode;
    }

    public String getDateFormat ()
    {
        return DateFormat;
    }

    public void setDateFormat (String DateFormat)
    {
        this.DateFormat = DateFormat;
    }

    public String getUnsubscribedProducts ()
    {
        return UnsubscribedProducts;
    }

    public void setUnsubscribedProducts (String UnsubscribedProducts)
    {
        this.UnsubscribedProducts = UnsubscribedProducts;
    }

    public String getPreferredLanguage ()
    {
        return PreferredLanguage;
    }

    public void setPreferredLanguage (String PreferredLanguage)
    {
        this.PreferredLanguage = PreferredLanguage;
    }

    public String getDefaultGroupCode ()
    {
        return DefaultGroupCode;
    }

    public void setDefaultGroupCode (String DefaultGroupCode)
    {
        this.DefaultGroupCode = DefaultGroupCode;
    }

    public String getImageFileType ()
    {
        return ImageFileType;
    }

    public void setImageFileType (String ImageFileType)
    {
        this.ImageFileType = ImageFileType;
    }

    public String getUTCOffset ()
    {
        return UTCOffset;
    }

    public void setUTCOffset (String UTCOffset)
    {
        this.UTCOffset = UTCOffset;
    }

    public String getTimeFormat ()
    {
        return TimeFormat;
    }

    public void setTimeFormat (String TimeFormat)
    {
        this.TimeFormat = TimeFormat;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [LoginID = "+LoginID+", UserType = "+UserType+", ImageFileName = "+ImageFileName+", DefaultGroupID = "+DefaultGroupID+", Password = "+Password+", UserName = "+UserName+", HoldingCompanyID = "+HoldingCompanyID+", HoldingCompanyCode = "+HoldingCompanyCode+", ImageFilePath = "+ImageFilePath+", DefaultPMSCUSTCODE = "+DefaultPMSCUSTCODE+", DefaultPMSCode = "+DefaultPMSCode+", DateFormat = "+DateFormat+", UnsubscribedProducts = "+UnsubscribedProducts+", PreferredLanguage = "+PreferredLanguage+", DefaultGroupCode = "+DefaultGroupCode+", ImageFileType = "+ImageFileType+", UTCOffset = "+UTCOffset+", TimeFormat = "+TimeFormat+"]";
    }
}
