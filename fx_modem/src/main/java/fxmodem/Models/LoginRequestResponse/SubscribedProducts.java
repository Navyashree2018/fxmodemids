package fxmodem.Models.LoginRequestResponse;

public class SubscribedProducts {

    private String ExpiryDate;

    private String ProductsCode;

    private String ProductID;

    private String Notification;

    public String getExpiryDate ()
    {
        return ExpiryDate;
    }

    public void setExpiryDate (String ExpiryDate)
    {
        this.ExpiryDate = ExpiryDate;
    }

    public String getProductsCode ()
    {
        return ProductsCode;
    }

    public void setProductsCode (String ProductsCode)
    {
        this.ProductsCode = ProductsCode;
    }

    public String getProductID ()
    {
        return ProductID;
    }

    public void setProductID (String ProductID)
    {
        this.ProductID = ProductID;
    }

    public String getNotification ()
    {
        return Notification;
    }

    public void setNotification (String Notification)
    {
        this.Notification = Notification;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [ExpiryDate = "+ExpiryDate+", ProductsCode = "+ProductsCode+", ProductID = "+ProductID+", Notification = "+Notification+"]";
    }

}
