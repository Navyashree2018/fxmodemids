package fxmodem.Models.LoginRequestResponse;

public class PropertySpecificrights {
    private String Description;

    private String Remarks;

    private String Value;

    private String Key;

    public String getDescription ()
    {
        return Description;
    }

    public void setDescription (String Description)
    {
        this.Description = Description;
    }

    public String getRemarks ()
    {
        return Remarks;
    }

    public void setRemarks (String Remarks)
    {
        this.Remarks = Remarks;
    }

    public String getValue ()
    {
        return Value;
    }

    public void setValue (String Value)
    {
        this.Value = Value;
    }

    public String getKey ()
    {
        return Key;
    }

    public void setKey (String Key)
    {
        this.Key = Key;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Description = "+Description+", Remarks = "+Remarks+", Value = "+Value+", Key = "+Key+"]";
    }
}
