package fxmodem.Models.LoginRequestResponse;


    public class UserAccessRights {
        private String StatusCode;

        private String Status;

        private String StatusDescription;

        private UserAccessRightsDetails Response;

        public String getStatusCode ()
        {
            return StatusCode;
        }

        public void setStatusCode (String StatusCode)
        {
            this.StatusCode = StatusCode;
        }

        public String getStatus ()
        {
            return Status;
        }

        public void setStatus (String Status)
        {
            this.Status = Status;
        }

        public String getStatusDescription ()
        {
            return StatusDescription;
        }

        public void setStatusDescription (String StatusDescription)
        {
            this.StatusDescription = StatusDescription;
        }


        public UserAccessRightsDetails getResponse() {
            return Response;
        }

        public void setResponse(UserAccessRightsDetails response) {
            Response = response;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [StatusCode = "+StatusCode+", Status = "+Status+", StatusDescription = "+StatusDescription+", Response = "+Response+"]";
        }
    }


