package fxmodem.Models.LoginRequestResponse;

import java.util.ArrayList;

public class UserRights {

    private String PropertyGroupCode;

    private String PmsCustCode;

    private ArrayList<Roles> Roles;

    public UserRights(String propertyGroupCode, String pmsCustCode, ArrayList<Roles> roles) {
        PropertyGroupCode = propertyGroupCode;
        PmsCustCode = pmsCustCode;
        Roles = roles;
    }

    public ArrayList<Roles> getRoles() {
        return Roles;
    }

    public void setRoles(ArrayList<Roles> roles) {
        Roles = roles;
    }



    public String getPropertyGroupCode ()
    {
        return PropertyGroupCode;
    }

    public void setPropertyGroupCode (String PropertyGroupCode)
    {
        this.PropertyGroupCode = PropertyGroupCode;
    }

    public String getPmsCustCode ()
    {
        return PmsCustCode;
    }

    public void setPmsCustCode (String PmsCustCode)
    {
        this.PmsCustCode = PmsCustCode;
    }


    @Override
    public String toString()
    {
        return "ClassPojo [PropertyGroupCode = "+PropertyGroupCode+", PmsCustCode = "+PmsCustCode+", Roles = "+Roles+"]";
    }
}
