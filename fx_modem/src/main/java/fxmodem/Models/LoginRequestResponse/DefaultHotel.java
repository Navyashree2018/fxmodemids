package fxmodem.Models.LoginRequestResponse;

public class DefaultHotel {

    private String HotelName;

    private String HotelCode;

    private String RecordStatus;

    private String Area;

    private String ID;

    private String City;

    public String getHotelName ()
    {
        return HotelName;
    }

    public void setHotelName (String HotelName)
    {
        this.HotelName = HotelName;
    }

    public String getHotelCode ()
    {
        return HotelCode;
    }

    public void setHotelCode (String HotelCode)
    {
        this.HotelCode = HotelCode;
    }

    public String getRecordStatus ()
    {
        return RecordStatus;
    }

    public void setRecordStatus (String RecordStatus)
    {
        this.RecordStatus = RecordStatus;
    }

    public String getArea ()
    {
        return Area;
    }

    public void setArea (String Area)
    {
        this.Area = Area;
    }

    public String getID ()
    {
        return ID;
    }

    public void setID (String ID)
    {
        this.ID = ID;
    }

    public String getCity ()
    {
        return City;
    }

    public void setCity (String City)
    {
        this.City = City;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [HotelName = "+HotelName+", HotelCode = "+HotelCode+", RecordStatus = "+RecordStatus+", Area = "+Area+", ID = "+ID+", City = "+City+"]";
    }
}
