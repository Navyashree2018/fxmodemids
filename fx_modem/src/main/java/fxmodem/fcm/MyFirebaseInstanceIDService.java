package fxmodem.fcm;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import fxmodem.Utils.LocalSharedStorage;

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {
    private static final String TAG = MyFirebaseInstanceIDService.class.getSimpleName();

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.i(TAG,"refreshed token/FCM ID ==>"+refreshedToken);
        storeRegIdInPref(refreshedToken);
    }

    private void storeRegIdInPref(String token) {
        LocalSharedStorage pref=new LocalSharedStorage(getApplicationContext());
        pref.save_GCM_Token(token);
    }
}
