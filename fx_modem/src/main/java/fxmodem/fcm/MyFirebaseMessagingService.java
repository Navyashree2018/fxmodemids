package fxmodem.fcm;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.idsnext.fxmodem.R;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import fxmodem.Activities.BaseActivity;
import fxmodem.Base.EnvironmentUrl;
import fxmodem.Base.MyApplication;
import fxmodem.Models.FCMPMSData.FCMPmsInfo;
import fxmodem.Models.FCMPMSData.PMSDataInterface;
import fxmodem.RoomDatabase.FCMPmsInfoDAO;
import fxmodem.RoomDatabase.FxModemRoomDB;
import fxmodem.Utils.CommonFunctions;
import fxmodem.Utils.FXMConstants;
import fxmodem.Gmail.GmailSender;
import fxmodem.Utils.LocalSharedStorage;
import fxmodem.db.LogDBManger;
import fxmodem.db.LogDBModel;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    public static String mesage, tille;
    LocalSharedStorage sharedPref;
    //FCMDBManager fcmdbManager;
    public static boolean PMSData = false;
    String deviceStatus;

    public String Date = "";
    public String EchoToken = "";
    public String NotificationType = "";
    public String NotificationData = "";
    public String NotificationDateTime = "";
    public String PmsRequest = "";
    public String PmsRequestData = "";
    public String PmsRequestDateTime = "";
    public String PmsResponseData = "";
    public String PmsResponseDateTime = "";
    public String FXRequest = "";
    public String FXRequestData = "";
    public String FXRequestDateTime = "";
    public String FXResponseData = "";
    public String FXResponseDateTime = "";
    public int count = 0;

    /*Data For Log*/
    public String FXApiFirstLog = "";
    public String FXRequestFirstLog = "";
    public String FXResponseFirstLog = "";
    public String PmsApiLog = "";
    public String PmsRequestLog = "";
    public String PmsResponseLog = "";
    public String FXApiLastLog = "";
    public String FXRequestLastLog = "";
    public String FXResponseLastLog = "";
    LogDBManger logDBManger;
    Boolean isLogMessaage = false;



    //private NotificationUtils notificationUtils;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        Log.i(MyFirebaseMessagingService.class.getSimpleName(), "MEssage received FxModem");
        Log.i(MyFirebaseMessagingService.class.getSimpleName(), "MEssage" + String.valueOf(remoteMessage));

        Log.i(MyFirebaseMessagingService.class.getSimpleName(), "MEssage" + remoteMessage.getMessageType());
        Log.i(MyFirebaseMessagingService.class.getSimpleName(), "MEssage" + remoteMessage.getMessageId());

        sharedPref = new LocalSharedStorage(getApplicationContext());
        logDBManger = new LogDBManger(getApplicationContext());

        Map<String, String> data = remoteMessage.getData();
        SaveNotificationData(remoteMessage.getData().get("Message"));

        FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);


        Bundle bundle = new Bundle();
        bundle.putString("NotificationTime", String.valueOf(Calendar.getInstance().getTime()));
        bundle.putString("NotificationData", remoteMessage.getData().get("Message"));
        mFirebaseAnalytics.logEvent("FxModemNotification", bundle);


        Bundle bundle1 = new Bundle();
        bundle1.putString(FirebaseAnalytics.Param.ITEM_ID,String.valueOf(Calendar.getInstance().getTime()) );
        bundle1.putString(FirebaseAnalytics.Param.ITEM_NAME, remoteMessage.getData().get("Message"));
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.VIEW_ITEM, bundle1);


        Date = "";
        EchoToken = "";
        NotificationType = "";
        NotificationData = "";
        NotificationDateTime = "";
        PmsRequest = "";
        PmsRequestData = "";
        PmsRequestDateTime = "";
        PmsResponseData = "";
        PmsResponseDateTime = "";
        FXRequest = "";
        FXRequestData = "";
        FXRequestDateTime = "";
        FXResponseData = "";
        FXResponseDateTime = "";

        if (CommonFunctions.isEmpty(EnvironmentUrl.scanFXPMSINterface())) {
            FXMConstants.SCAN_IP_VALUE = sharedPref.getIPAddress().trim().replace(" ", "");
        }

        /*if (remoteMessage.getNotification() != null) {
           if(remoteMessage.getNotification().getBody().equalsIgnoreCase("PrintLog") || (remoteMessage.getNotification().getTitle().equalsIgnoreCase("PrintLog"))){
               isLogMessaage = true;
               sendMailInBackground(this);
              // sendLogMail();
           }
        }*/

        if (data != null) {
            mesage = data.get("Message");
            tille = data.get("Title");
            String messagetype = data.get("NotificationType");
            Log.e("jjjjjjjjjjjjjjjjjjjjj","MsgType...."+messagetype);
            Log.i(MyFirebaseMessagingService.class.getSimpleName(), "Message Data==>>" + mesage);
            Log.i(MyFirebaseMessagingService.class.getSimpleName(), "Title Data==>>" + tille);


            if (messagetype != null && messagetype.equalsIgnoreCase(FXMConstants.MESSAGE_TYPE_UPDATE)) {
                return;
            }

            //fcmdbManager = new FCMDBManager(getApplicationContext());
            Date = CommonFunctions.getDate();
            NotificationType = tille;
            NotificationData = mesage;
            NotificationDateTime = CommonFunctions.getDateTime();

            try {
                JSONObject jsonParamsmDevice = new JSONObject(mesage);
                String url = EnvironmentUrl.scanFXPMSINterface() + tille + "/" + sharedPref.getPMSCustCode();
                String ectoken = jsonParamsmDevice.getString("EchoToken");
                Log.i(MyFirebaseMessagingService.class.getSimpleName(), "Echo token Data==>>" + ectoken);


               /* if (mesage!=null && !CommonFunctions.isEmpty(mesage)){
                    callAgainsendDatatoPMS("Got push","",PMSData,bb,true);
                }*/

                PmsRequest = url;
                EchoToken = ectoken;
                PmsRequestData = mesage;
                PmsRequestDateTime = CommonFunctions.getDateTime();

                saveFCMDBData(getApplicationContext());

            } catch (Exception e) {
            }


            /*After getting push immediately call salman api....
              whether success/failure call ravi api then again call salman api*/
           // callAgainsendDatatoPMS("","Got Notification", PMSData, EchoToken,true);
           //Commented above thing as its just for log purpose
             sendDatatoPMSGotPush(mesage, tille);


            /*Intent intent = new Intent(this, Room_Upgrad_List_Activity.class);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                    PendingIntent.FLAG_ONE_SHOT);*/

            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.mipmap.fc_logo)
                    .setLargeIcon(BitmapFactory.decodeResource(this.getResources(), R.mipmap.ic_launcher))
                    .setContentTitle(tille)
                    .setContentText(mesage)
                    .setAutoCancel(true)
                    .setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE);
            //.setContentIntent(pendingIntent);

            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);


        } else {
            /*Intent intent = new Intent(this, Room_Upgrad_List_Activity.class);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                    PendingIntent.FLAG_ONE_SHOT);*/

            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.mipmap.fc_logo)
                    .setLargeIcon(BitmapFactory.decodeResource(this.getResources(), R.mipmap.ic_launcher))
                    .setContentTitle("New Message")
                    .setContentText("Check new Message")
                    .setAutoCancel(true)
                    .setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE);
            //.setContentIntent(pendingIntent);

            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        }
    }

    //ravi api
    /*
    * Retry logic is implemented
    * */
    private void sendDatatoPMSGotPush(final String mesage, final String title) {
        Log.i(MyFirebaseMessagingService.class.getSimpleName(), "sendDatatoPMSGotPush : ip value" + FXMConstants.SCAN_IP_VALUE);
        try {
            JSONObject jsonParamsmDevice = new JSONObject(mesage);
            final String jsonParamsmDeviceReq = jsonParamsmDevice.toString();
            final String bb = jsonParamsmDevice.getString("EchoToken");
            Log.i(MyFirebaseMessagingService.class.getSimpleName(), "sendData.toPMSGotPush : jsonReq" + jsonParamsmDeviceReq.toString());

            PmsApiLog = EnvironmentUrl.scanFXPMSINterface() + title + "/" + sharedPref.getPMSCustCode();
            PmsRequestLog = jsonParamsmDeviceReq;
            BaseActivity.VolleyCallback1 callback = new BaseActivity.VolleyCallback1() {
                @Override
                public void onSuccess(String result) {

                    PmsResponseLog = result;
                    Log.i(MyFirebaseMessagingService.class.getSimpleName(), "sendDatatoPMSGotPush : result" + result.toString());
                    if (result != null && result.length() > 0) {
                        try {


                            JSONObject jsonData = new JSONObject(result);
                            deviceStatus = jsonData.optString("Status", "");
                            String statusDescription = jsonData.optString("Description", "");
                           /* ToastShort(getApplicationContext(), deviceStatus);
                            ToastShort(getApplicationContext(), statusDescription);*/
                            PmsResponseData = jsonData.toString();
                            PmsResponseLog = result;
                            PmsResponseDateTime = CommonFunctions.getDateTime();
                            Log.i(MyFirebaseMessagingService.class.getSimpleName(), "sendDatatoPMSGotPush :PmsResponseData ==>>:" + PmsResponseData);

                            callAgainsendDatatoPMS(result, statusDescription, PMSData, bb, false);



                           /* if (deviceStatus != null && deviceStatus.length() > 0) {
                                if (deviceStatus.equalsIgnoreCase("Success")) {
                                    ToastShort(getApplicationContext(),"Calling Salman API");

                                    PMSData=true;
                                    callAgainsendDatatoPMS(deviceStatus,statusDescription,PMSData,bb);
                                   // Toast.makeText(getApplicationContext(), "success", Toast.LENGTH_SHORT).show();
                                }
                            }

                            if (deviceStatus.equalsIgnoreCase("Failure")) {
                                PMSData=false;
                                ToastShort(getApplicationContext(),"Calling Salman API");

                                callAgainsendDatatoPMS(deviceStatus,statusDescription,PMSData,bb);

                            }*/

                            if (deviceStatus.equalsIgnoreCase("Failure")) {
                                count++;
                                if(count == 1)
                                    sendDatatoPMSGotPush(mesage,title);
                                else
                                    callAgainsendDatatoPMS("Api Fail", result, PMSData, bb, false);

                                sendFailureMail("Request:::\n"+jsonParamsmDeviceReq+"\nResponse::::\n"+result);
                            }
                        } catch (Exception e) {
                            Log.i(MyFirebaseMessagingService.class.getSimpleName(), "sendDatatoPMSGotPush :exception ==>>:" + e.toString());
                        }
                    } else {
                        count++;
                        if(count == 1)
                            sendDatatoPMSGotPush(mesage,title);
                        else
                            callAgainsendDatatoPMS("Api Fail", result, PMSData, bb, false);

                        Log.i(MyFirebaseMessagingService.class.getSimpleName(), "sendDatatoPMSGotPush : API Failed");
                        sendFailureMail("Request:::\n"+jsonParamsmDeviceReq+"\nResponse::::\n"+result);
                    }
                }

                @Override
                public void onError(VolleyError result) {
                    try {
                        String errorMsg = result.toString() +"-"+result.networkResponse.statusCode;
                        PmsResponseLog = errorMsg;
                        count++;
                        if(count == 1)
                            sendDatatoPMSGotPush(mesage,title);
                        else
                            callAgainsendDatatoPMS("Api Fail", result.toString(), PMSData, bb, false);

                        Log.i(MyFirebaseMessagingService.class.getSimpleName(), "sendDatatoPMSGotPush : onerror" + result);
                        sendFailureMail("Request:::\n"+jsonParamsmDeviceReq+"\nResponse::::\n"+errorMsg);
                    } catch (Exception e) {
                        // ToastShort(getApplicationContext(), "Please provide valid ip");
                        e.printStackTrace();
                    }
                }
            };

            volleyPostRequest(EnvironmentUrl.scanFXPMSINterface() + title + "/" + sharedPref.getPMSCustCode(), jsonParamsmDeviceReq, callback);
        } catch (Exception g) {
            g.printStackTrace();
        }
    }

    private void sendDatatoPMS(String mesage, String title) {

        Log.i(MyFirebaseMessagingService.class.getSimpleName(), "sendDatatoPMS : ip value" + FXMConstants.SCAN_IP_VALUE);
        try {
            JSONObject jsonParamsmDevice = new JSONObject(mesage);
            String jsonParamsmDeviceReq = jsonParamsmDevice.toString();
            String bb = jsonParamsmDevice.getString("EchoToken");
            Log.i(MyFirebaseMessagingService.class.getSimpleName(), "sendDatatoPMS : jsonReq" + jsonParamsmDeviceReq.toString());

            BaseActivity.VolleyCallback1 callback = new BaseActivity.VolleyCallback1() {
                @Override
                public void onSuccess(String result) {

                    Log.i(MyFirebaseMessagingService.class.getSimpleName(), "sendDatatoPMS : result" + result.toString());
                    if (result != null && result.length() > 0) {
                        try {


                            JSONObject jsonData = new JSONObject(result);
                            deviceStatus = jsonData.optString("Status", "");
                            String statusDescription = jsonData.optString("Description", "");
                         /*   ToastShort(getApplicationContext(), deviceStatus);
                            ToastShort(getApplicationContext(), statusDescription);*/
                            PmsResponseData = jsonData.toString();
                            PmsResponseDateTime = CommonFunctions.getDateTime();
                            Log.i(MyFirebaseMessagingService.class.getSimpleName(), "sendDatatoPMS :PmsResponseData ==>>:" + PmsResponseData);


                        } catch (Exception e) {
                            Log.i(MyFirebaseMessagingService.class.getSimpleName(), "sendDatatoPMS :exception ==>>:" + e.toString());
                        }
                    } else {
                        Log.i(MyFirebaseMessagingService.class.getSimpleName(), "sendDatatoPMS : API Failed");
                    }
                }

                @Override
                public void onError(VolleyError result) {
                    try {
                        //   ToastShort(getApplicationContext(), "Ravi Api Error");
                        Log.i(MyFirebaseMessagingService.class.getSimpleName(), "sendDatatoPMS : onerror" + result.toString());
                    } catch (Exception e) {
                        //  ToastShort(getApplicationContext(), "Please provide valid ip");
                        e.printStackTrace();
                    }
                }

            };

            volleyPostRequest(EnvironmentUrl.scanFXPMSINterface() + title + "/" + sharedPref.getPMSCustCode(), jsonParamsmDeviceReq, callback);
        } catch (Exception g) {
            g.printStackTrace();
        }
    }

    //salman api
    private void callAgainsendDatatoPMS(String result, final String statusDescription, final boolean pmsData, final String bb, final boolean isFirstTime) {

        PMSDataInterface pmsDataInterface = new PMSDataInterface();
        pmsDataInterface.setPMSCustCode(sharedPref.getPMSCustCode());
        pmsDataInterface.setRequestID(bb);
        pmsDataInterface.setResponseData(result);
        pmsDataInterface.setResponseStatus(deviceStatus);
        pmsDataInterface.setResponseRemarks(statusDescription);

        Gson gson = new Gson();
        String jsonParamsmDeviceReq1 = gson.toJson(pmsDataInterface);

        FXRequest = EnvironmentUrl.getReplyPMSData();
        FXRequestData = jsonParamsmDeviceReq1;
        FXRequestDateTime = CommonFunctions.getDateTime();

        if(isFirstTime){
            FXApiFirstLog = FXRequest;
            FXRequestFirstLog = FXRequestData;
        }
        else{
            FXApiLastLog = FXRequest;
            FXRequestLastLog = FXRequestData;
        }

        Log.i(MyFirebaseMessagingService.class.getSimpleName(), "getReplyPMSData - URL==>> : \n" + EnvironmentUrl.getReplyPMSData());
        Log.i(MyFirebaseMessagingService.class.getSimpleName(), "getReplyPMSData : jsonrequest ==>>" + jsonParamsmDeviceReq1);
        BaseActivity.VolleyCallback1 callback = new BaseActivity.VolleyCallback1() {
            @Override
            public void onSuccess(String result) {
                //sendDatatoPMS(mesage,tille);

                if (result != null && result.length() > 0) {
                    try {
                        Log.i(MyFirebaseMessagingService.class.getSimpleName(), "getReplyPMSData - result : \n" + result.toString());
                        JSONObject jsonData = new JSONObject(result);
                        String deviceStatus = jsonData.optString("Status", "");
                        String statusDescription = jsonData.optString("StatusDescription", "");
                        if (deviceStatus != null && deviceStatus.length() > 0) {
                            FXResponseData = jsonData.toString();
                            FXResponseDateTime = CommonFunctions.getDateTime();
                            //  ToastShort(getApplicationContext(), "Success From Salman Api");

                            sendMailAutimatically();
                            if(isFirstTime){
                                sendDatatoPMSGotPush(mesage, tille);
                                FXResponseFirstLog = result;
                            }
                            else{
                                FXResponseLastLog = result;
                                SaveLogData();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    //  ToastShort(getApplicationContext(),"Salman Api empty result....");
                    sendFailureMail("Salman Api empty result");
                    if(isFirstTime){
                        sendDatatoPMSGotPush(mesage, tille);
                        FXResponseFirstLog = result;
                    }
                    else{
                        FXResponseLastLog = result;
                        SaveLogData();
                    }
                }
            }

            @Override
            public void onError(VolleyError result) {
                // sendDatatoPMS(mesage,tille);
                /*Send Mail with proper error*/

                String errorMsg = result.toString() +"-"+result.networkResponse.statusCode;
                sendFailureMail(errorMsg);
                if(isFirstTime){
                    sendDatatoPMSGotPush(mesage, tille);
                    FXResponseFirstLog = errorMsg;
                }
                else{
                    FXResponseLastLog = errorMsg;
                    SaveLogData();
                }
                Log.i(MyFirebaseMessagingService.class.getSimpleName(), "getReplyPMSData - onerror : \n" + errorMsg);
                // ToastShort(getApplicationContext(),"Salman Api Fail....");
            }
        };
        volleyPostRequest(EnvironmentUrl.getReplyPMSData(), jsonParamsmDeviceReq1, callback);

    }

    private void sendMailAutimatically() {
        //String dbData = fcmdbManager.getJSON("", false);



        /*final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setTitle("Sending Email");
        dialog.setMessage("Please wait");
        dialog.show();*/
        /*Thread sender = new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    FCMPmsInfoDAO dao = FxModemRoomDB.INSTANCE.wordDao();
                    FCMPmsInfo dbData1 = dao.getPmsInfoFromDB();
                    final String dbData = dbData1.toString();


                    GmailSender sender = new GmailSender("demoidsnext@gmail.com", "demoids@1234");
                    sender.sendMail("FCM DB Interface Data",
                            dbData,
                            "demoidsnext@gmail.com",
                            "demoidsnext@gmail.com");
                    //dialog.dismiss();
                    //   ToastShort(getApplicationContext(), "Sent Mail Successfully");

                    Log.i(MyFirebaseMessagingService.class.getSimpleName(), " mail sent successfully: " + dbData.toString());

                } catch (Exception e) {
                    Log.i(MyFirebaseMessagingService.class.getSimpleName(), "Error sending mail: " + e.getMessage());
                }
            }
        });
        sender.start();*/

        Thread sender = new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    FCMPmsInfoDAO dao = FxModemRoomDB.INSTANCE.wordDao();
                    FCMPmsInfo dbData1 = dao.getPmsInfoFromDB();
                    final String dbData = dbData1.toString();


                    GmailSender sender = new GmailSender("demoidsnext@gmail.com", "demoids@1234");
                    sender.sendMail("FCM DB Interface Data",
                            dbData,
                            "demoidsnext@gmail.com",
                            "demoidsnext@gmail.com");
                    //dialog.dismiss();
                    //   ToastShort(getApplicationContext(), "Sent Mail Successfully");

                    Log.i(MyFirebaseMessagingService.class.getSimpleName(), "Failure mail sent successfully: " + dbData.toString());

                } catch (Exception e) {
                    Log.i(MyFirebaseMessagingService.class.getSimpleName(), "Failure Error sending mail: " + e.getMessage());
                }
            }
        });
        sender.start();
    }

    private void sendFailureMail(final String result) {
        //String dbData = fcmdbManager.getJSON("", false);



        /*final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setTitle("Sending Email");
        dialog.setMessage("Please wait");
        dialog.show();*/
        Thread sender = new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    FCMPmsInfoDAO dao = FxModemRoomDB.INSTANCE.wordDao();
                    FCMPmsInfo dbData1 = dao.getPmsInfoFromDB();
                    final String dbData = dbData1.toString();


                    GmailSender sender = new GmailSender("demoidsnext@gmail.com", "demoids@1234");
                    sender.sendMail("PMS Interface Failed",
                            result,
                            "demoidsnext@gmail.com",
                            "demoidsnext@gmail.com");
                    //dialog.dismiss();
                    //   ToastShort(getApplicationContext(), "Sent Mail Successfully");

                    Log.i(MyFirebaseMessagingService.class.getSimpleName(), "Failure mail sent successfully: " + dbData.toString());

                } catch (Exception e) {
                    Log.i(MyFirebaseMessagingService.class.getSimpleName(), "Failure Error sending mail: " + e.getMessage());
                }
            }
        });
        sender.start();
    }

   /* private void saveFCMDBData() {
        fcmdbManager.insertFCMDBData(Date, EchoToken, NotificationType, NotificationData, NotificationDateTime,
                PmsRequest, PmsRequestData, PmsRequestDateTime, PmsResponseData, PmsResponseDateTime, FXRequest,
                FXRequestData, FXRequestDateTime, FXResponseData, FXResponseDateTime);

        constructJSON(CommonFunctions.getDate());
    }

    private void constructJSON(String date) {
        String jsonFromDataBase = fcmdbManager.getJSON(date, false);
        Log.i("VOLLEY", "Volley Json From DB " + jsonFromDataBase);
    }*/

    /**
     * Showing notification with text only
     */
    private void showNotificationMessage(Context context, String title, String message, String timeStamp, Intent intent) {
       /* notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent);*/
    }

    /**
     * Showing notification with text and image
     */
    private void showNotificationMessageWithBigImage(Context context, String title, String message, String timeStamp, Intent intent, String imageUrl) {
       /* notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent, imageUrl);*/
    }

    private void testPushMsg(String msg, String title) {
        try {

            if (msg == null) {
                msg = "new Notification from Front Office";
            }

            Notification notif = new NotificationCompat.Builder(getApplicationContext()).setContentTitle(title)
                    .setContentText(msg)
                    .setSmallIcon(R.drawable.ic_alert)
                    .build();
            NotificationManagerCompat manager = NotificationManagerCompat.from(getApplicationContext());
            manager.notify(0, notif);
        } catch (Exception e) {
        }
    }

    public void volleyPostRequest(final String mUrl, final String jsonParams, final BaseActivity.VolleyCallback1 callback) {

        Log.i(MyFirebaseMessagingService.class.getSimpleName(), "mUrl: " + mUrl);
        Log.i(MyFirebaseMessagingService.class.getSimpleName(), "jsonParams: " + jsonParams);
        try {
            String tag_json_obj = "json_obj_req";
            //RequestQueue queue = Volley.newRequestQueue(this);
            JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, mUrl,
                    new JSONObject(jsonParams),
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                /*Log.i("VOLLEY", "\n\n\n*******************************************************************");
                                Log.i("VOLLEY", "\nURL: " + mUrl);
                                Log.i("VOLLEY", "\njsonParams: " + jsonParams);*/
                                Log.i("VOLLEY", "\nResponse : " + response.toString());


                               /* Log.i(MyFirebaseMessagingService.class.getSimpleName(), "\n\n\n*******************************************************************");
                                Log.i(MyFirebaseMessagingService.class.getSimpleName(), "\nURL: " + mUrl);
                                Log.i(MyFirebaseMessagingService.class.getSimpleName(), "\njsonParams: " + jsonParams);
                                Log.i(MyFirebaseMessagingService.class.getSimpleName(), "\nResponse : " + response.toString());
                             */   callback.onSuccess(response.toString());
                            } catch (JsonSyntaxException e) {
                                e.printStackTrace();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            //Get response code here
                            error.printStackTrace();
                            if (error != null) {
                                try {
                                    /*Log.i("VOLLEY", "\n\n\n*******************************************************************");
                                    Log.i("VOLLEY", "\nURL:\n " + mUrl);
                                    Log.i("VOLLEY", "\njsonParams: \n" + jsonParams);
                                 */   Log.i("VOLLEY", "\nResponse :\n\n " + error.networkResponse.statusCode);
                                   // Log.i("TAG", "VolleyError  - networkResponse.statusCode : " + error.networkResponse.statusCode + "\n mUrl: " + mUrl); Log.i("VOLLEY", "\n\n\n*******************************************************************");


                                   /* Log.i(MyFirebaseMessagingService.class.getSimpleName(), "\nURL:\n " + mUrl);
                                    Log.i(MyFirebaseMessagingService.class.getSimpleName(), "\njsonParams: \n" + jsonParams);
                                    Log.i(MyFirebaseMessagingService.class.getSimpleName(), "\nResponse :\n\n " + error.networkResponse.statusCode);
                                    Log.i(MyFirebaseMessagingService.class.getSimpleName(), "VolleyError  - networkResponse.statusCode : " + error.networkResponse.statusCode + "\n mUrl: " + mUrl);
                               */ } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                            Log.e("TAG", "VolleyError  : " + error);
                            callback.onError(error);
                        }
                    }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                   // headers.put("Content-Type", "application/json; charset=utf-8");
                    headers.put("Authorization", "bearer " + sharedPref.getAccessToken());
                    return headers;
                }
            };
            //DiskBasedCache cache = new DiskBasedCache(getCacheDir(), 16 * 1024 * 1024);
            //queue = new RequestQueue(cache, new BasicNetwork(new HurlStack()));
            //queue.start();
            // clear all volley caches.
            //queue.add(new ClearCacheRequest(cache, null));
            int socketTimeout = 20000;//50 seconds - change to what you want
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postRequest.setRetryPolicy(policy);
            // Adding request to request queue
            Log.d("TAG", "volleyPostRequest - postRequest :\n " + postRequest + "\ntag_json_obj: " + tag_json_obj);
            MyApplication.getInstance().addToRequestQueue(postRequest, tag_json_obj);
            //queue.add(postRequest);

            Log.d("TAG", "volleyPostRequest - postRequest : " + postRequest);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("TAG", "volleyPostRequest - Exception : ");
        }
    }

    private void saveFCMDBData(final Context ctx) {
        try {
            new AsyncTask<Void, Void, String>() {
                @Override
                protected String doInBackground(Void... params) {
                    //creating a task
                    FCMPmsInfo datamodel = new FCMPmsInfo();
                    datamodel.setDate(Date);
                    datamodel.setEcho_Token(EchoToken);
                    datamodel.setNotification_Type(NotificationType);
                    datamodel.setNotification_Data(NotificationData);
                    datamodel.setNotification_Date_Time(NotificationDateTime);
                    datamodel.setPms_Request(PmsRequest);
                    datamodel.setPms_Request_Data(PmsRequestData);
                    datamodel.setPms_Request_Date_Time(PmsRequestDateTime);
                    datamodel.setPms_Response_Data(PmsResponseData);
                    datamodel.setPms_Response_Date_Time(PmsResponseDateTime);
                    datamodel.setFX_Request(FXRequest);
                    datamodel.setFX_Request_Data(FXRequestData);
                    datamodel.setFX_Request_Date_Time(FXRequestDateTime);
                    datamodel.setFX_Response_Data(FXResponseData);
                    datamodel.setFX_Response_Date_Time(FXResponseDateTime);


                    //adding to database
          /*  FxModemRoomDB.getDatabase(getApplicationContext()).getAppDatabase()
                    .taskDao()
                    .insert(task);*/
                    FCMPmsInfoDAO dao = FxModemRoomDB.getDatabase(getApplicationContext()).wordDao();
                    dao.deleteAll();

                    dao.insert(datamodel);


                    return null;
                }

                @Override
                protected void onPostExecute(String gcmId) {
                    Toast.makeText(getApplicationContext(), "Saved", Toast.LENGTH_LONG).show();

                }
            }.execute(null, null, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void SaveLogData(){
        //logDBManger.deleteLogTableData();
        logDBManger.insertLogDBData(CommonFunctions.getDate(),FXApiFirstLog,FXRequestFirstLog,FXResponseFirstLog,
                PmsApiLog,PmsRequestLog,PmsResponseLog,FXApiLastLog,FXRequestLastLog,FXResponseLastLog);

    }

    private void SaveNotificationData(String message){
        logDBManger.insertLogDBData(CommonFunctions.getDate(),"--" ,"--","--",
                "Notification Received :::::\n"+String.valueOf(Calendar.getInstance().getTime()),"RemoteMessage :::: \n"+message,"--","--","--","--");

    }

    private void sendMailInBackground(final Context ctx) {
        try {
            new AsyncTask<Void, Void, String>() {
                @Override
                protected String doInBackground(Void... params) {

                    Thread sender = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {

                                LogDBManger logDBManger = new LogDBManger(getApplicationContext());
                                ArrayList<LogDBModel> logDBModelList = logDBManger.getLog();


                                GmailSender sender = new GmailSender("demoidsnext@gmail.com", "demoids@1234");


                                sender.sendMail("Logs :"+sharedPref.getPMSCustCode()+"\n",
                                        logDBModelList.toString()+"",
                                        "demoidsnext@gmail.com",
                                        "demoidsnext@gmail.com");

                                //dialog.dismiss();
                                //   ToastShort(getApplicationContext(), "Sent Mail Successfully");

                                Log.i(MyFirebaseMessagingService.class.getSimpleName(), "Logs mail sent successfully: " );

                            } catch (Exception e) {
                                Log.i(MyFirebaseMessagingService.class.getSimpleName(), "Logs Error sending mail: " + e.getMessage());
                            }
                        }
                    });
                    sender.start();


                    return null;
                }

                @Override
                protected void onPostExecute(String gcmId) {
                    Toast.makeText(getApplicationContext(), "Saved", Toast.LENGTH_LONG).show();

                }
            }.execute(null, null, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendLogMail() {

        Thread sender = new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    LogDBManger logDBManger = new LogDBManger(getApplicationContext());
                    ArrayList<LogDBModel> logDBModelList = logDBManger.getLog();


                    GmailSender sender = new GmailSender("demoidsnext@gmail.com", "demoids@1234");


                    sender.sendMail("Logs :"+sharedPref.getPMSCustCode()+"\n",
                            logDBModelList.toString()+"",
                            "demoidsnext@gmail.com",
                            "demoidsnext@gmail.com");

                    //dialog.dismiss();
                    //   ToastShort(getApplicationContext(), "Sent Mail Successfully");

                    Log.i(MyFirebaseMessagingService.class.getSimpleName(), "Logs mail sent successfully: " );

                } catch (Exception e) {
                    Log.i(MyFirebaseMessagingService.class.getSimpleName(), "Logs Error sending mail: " + e.getMessage());
                }
            }
        });
        sender.start();
    }

}









