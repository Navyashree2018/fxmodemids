package fxmodem.Base;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Intent;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;

import androidx.core.content.ContextCompat;
import androidx.multidex.MultiDexApplication;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import net.danlew.android.joda.JodaTimeAndroid;
import fxmodem.AppActivatingService;
import fxmodem.Receivers.InternetConnectivityReceiver;


public class MyApplication extends MultiDexApplication {

    public static final String TAG = MyApplication.class.getSimpleName();

    private RequestQueue mRequestQueue;
    public static MyApplication mInstance;
    public static final String CHANNEL_ID = "FxModem";




    @Override
    public void onCreate() {


        super.onCreate();
        mInstance = this;
        startService();
        JodaTimeAndroid.init(this);
        Log.i("TAG","****************************"+mInstance);
        createNotificationChannel();

        // myPort = ConnectionUtils.getPort(getApplicationContext());
        //connListener = new ConnectionListener(getApplicationContext(), myPort);
        //this.registerReceiver(new InternetConnectivityReceiver(), new IntentFilter(Intent.ACTION_BATTERY_CHANGED));


    }


    public void startService() {
        Intent serviceIntent = new Intent(this, AppActivatingService.class);
        serviceIntent.putExtra("inputExtra", "start");
        ContextCompat.startForegroundService(this, serviceIntent);
    }

    public static synchronized MyApplication getInstance() {
        return mInstance;
    }


    public <T> void addToRequestQueue(Request<T> req, String tag) {
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }



    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel serviceChannel = new NotificationChannel(
                    CHANNEL_ID,
                    "FxModem Connected ",
                    NotificationManager.IMPORTANCE_DEFAULT
            );
            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(serviceChannel);
        }
    }
}
