package fxmodem.Base;

import fxmodem.Utils.CommonFunctions;
import fxmodem.Utils.FXMConstants;

public class EnvironmentUrl {


    public static String getFCMLogURL() {
        if (!CommonFunctions.isEmpty(getServerURL()))
            return getServerURL() + "DashBoard/Sessionlog";
        else
            return null;
    }

    public static String setFCM_message_key() {
        return "https://dockwebapi.azurewebsites.net/api/dock/v1/user/savelogindetails";
    }

    public static String getReplyPMSData() {
        if (!CommonFunctions.isEmpty(getServerURL()))
            return getServerURL() + "POSInterface/NotificationResponce";
        else
            return null;
    }

    public static String scanFXPMSINterface() {
        String ip = FXMConstants.SCAN_IP_VALUE;
        if (CommonFunctions.isEmpty(ip)) {

            return null;
        } else {
            return "http://" + ip +"/";//"http://172.16.8.35:9999/Scan";
        }
    }

    public static String testIPConnectivityForScan() {
        String ip = FXMConstants.SCAN_IP_VALUE;
        if (CommonFunctions.isEmpty(ip)) {
            return null;
        } else {
            return "http://" + ip +"/testconnectivity";//http://172.16.8.90/FXPMSInterface/testconnectivity
        }
    }


    public static String getUserLoginData() {
        if (!CommonFunctions.isEmpty(getServerURL()))
            return getServerURL() + "User/LogIn";
        else
            return null;
    }

    public static String getToken() {
        if (!CommonFunctions.isEmpty(getServerURL()))
            return getServerURL() + "token";
        else
            return null;
    }

    public static String getServerURL() {
        if (FXMConstants.SERVER_ENV.equals(FXMConstants.ENV_DEV))
            return FXMConstants.serverBaseDevURL;
        else if (FXMConstants.SERVER_ENV.equals(FXMConstants.ENV_MOBILE))
            return FXMConstants.serverBaseMobileURL;
        else if (FXMConstants.SERVER_ENV.equals(FXMConstants.ENV_QA))
            return FXMConstants.serverBaseQAURL;
        else if (FXMConstants.SERVER_ENV.equals(FXMConstants.ENV_UA))
            return FXMConstants.serverBaseUAURL;
        else if (FXMConstants.SERVER_ENV.equals(FXMConstants.ENV_FAT))
            return FXMConstants.serverBaseFatURL;
        else if (FXMConstants.SERVER_ENV.equals(FXMConstants.ENV_PMS))
            return FXMConstants.serverBasePMSURL;
        else if (FXMConstants.SERVER_ENV.equals(FXMConstants.ENV_PMSSYNC))
            return FXMConstants.serverBasePMSSYNCURL;
        else if (FXMConstants.SERVER_ENV.equals(FXMConstants.ENV_LIVE))
            return FXMConstants.serverBaseLiveURL;
        else if (FXMConstants.SERVER_ENV.equals(FXMConstants.ENV_STAGING))
            return FXMConstants.serverBaseStagingURL;
        else if (FXMConstants.SERVER_ENV.equals(FXMConstants.ENV_STAGING_SECOND))
            return FXMConstants.serverBaseStagingSecondURL;
        else
            return null;
    }

}
