package fxmodem.RoomDatabase;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import fxmodem.Models.FCMPMSData.FCMPmsInfo;

@Dao
public interface FCMPmsInfoDAO {

    // allowing the insert of the same word multiple times by passing a
    // conflict resolution strategy
    @Insert(onConflict = OnConflictStrategy.IGNORE)
     void insert(FCMPmsInfo pmsdata);

    @Query("DELETE FROM fcm_pms_info")
    void deleteAll();

    @Query("SELECT * from fcm_pms_info")
    FCMPmsInfo getPmsInfoFromDB();

    @Query("SELECT * from fcm_pms_info WHERE Date=:date")
    FCMPmsInfo getPmsInfoFromDBforDate(String date);

    @Query("SELECT * FROM fcm_pms_info WHERE Echo_Token = :param")
    FCMPmsInfo FindFCmId(String param);
}