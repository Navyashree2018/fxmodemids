package fxmodem.Activities;

import android.app.Dialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;

import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.idsnext.fxmodem.R;

import org.json.JSONObject;

import fxmodem.AppActivatingService;
import fxmodem.Base.EnvironmentUrl;
import fxmodem.Gmail.GmailSender;
import fxmodem.Models.FCMPMSData.FCMPmsInfo;
import fxmodem.Models.FCMParam;
import fxmodem.RoomDatabase.FCMPmsInfoDAO;
import fxmodem.RoomDatabase.FxModemRoomDB;
import fxmodem.Utils.CommonFunctions;
import fxmodem.Utils.CustomProgressBar;
import fxmodem.Utils.FXMConstants;
import fxmodem.fcm.MyFirebaseMessagingService;

public class DashboardActivity extends BaseActivity {

    private static final String TAG = DashboardActivity.class.getSimpleName();
    public String ipAddr = "";
    LinearLayout beforeConnection, afterConnection, ll_logout;
    TextView accesstoken, tv_ipval, tv_edit;
    EditText et_loginIP;
    private CustomProgressBar customProgressBar;
    FrameLayout fl_log;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        initializeObject();

        FirebaseApp.initializeApp(DashboardActivity.this);
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        sharedPref.save_GCM_Token(refreshedToken);

        sendGCMCredential_Dock();
        sendGCMCredentialToServer();

        //startService(new Intent(this, AppActivatingService.class));
        //startService();
        sendIpReconnectMail();

        initViews();

    }


    @Override
    public void onResume() {
        super.onResume();
        if (CommonFunctions.isEmpty(FXMConstants.SCAN_IP_VALUE)) {
            if (!CommonFunctions.isEmpty(sharedPref.getIPAddress())) {
                FXMConstants.SCAN_IP_VALUE = sharedPref.getIPAddress();
                testIPConnection();
            }
        } else {
            testIPConnection();

        }

        CommonFunctions.isLogout = true;
        ;

    }

    private void initializeObject() {
        customProgressBar = new CustomProgressBar(DashboardActivity.this);
        getSupportActionBar().hide();
        beforeConnection = (LinearLayout) findViewById(R.id.beforeConnection);
        afterConnection = (LinearLayout) findViewById(R.id.afterConnection);
        ll_logout = (LinearLayout) findViewById(R.id.ll_logout);
        accesstoken = (TextView) findViewById(R.id.accessId);
        tv_ipval = (TextView) findViewById(R.id.ipvalue);
        tv_edit = (TextView) findViewById(R.id.tv_edit);
        et_loginIP = (EditText) findViewById(R.id.ed_loginip);
        et_loginIP.setText(sharedPref.getIPAddress());
        fl_log = findViewById(R.id.fl_log);

        // this.registerReceiver(this.mBatInfoReceiver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));

        accesstoken.setTextIsSelectable(true);
        registerForContextMenu(accesstoken);

    }

    private void initViews() {

        setConnectionLayout();
        findViewById(R.id.btnProceed).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (!isNetworkConnected()) {
                    ToastShort(DashboardActivity.this, getResources().getString(R.string.no_internet));
                    return;
                }

                ipAddr = et_loginIP.getText().toString().replace(" ", "").trim();
                FXMConstants.SCAN_IP_VALUE = ipAddr;


                if (CommonFunctions.isEmpty(ipAddr)) {
                    Toast.makeText(DashboardActivity.this, getResources().getString(R.string.empty_ipAddr), Toast.LENGTH_SHORT).show();
                } else {
                    testIPConnection();
                }
            }


        });

        ll_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showDialogForLogout(0);
            }
        });

        tv_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setConnectionLayout();
            }
        });

        fl_log.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(DashboardActivity.this, LogDBActivity.class));
            }
        });
    }

    private void onLogoutSuccess() {

        Intent intent = new Intent(DashboardActivity.this, LoginActivity.class);
        sharedPref.saveLoginBoolen(false);
        intent.putExtra("Username", sharedPref.getUserId());
        CommonFunctions.isLogout = false;


        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {

                FCMPmsInfoDAO dao = FxModemRoomDB.getDatabase(getApplicationContext()).wordDao();
                dao.deleteAll();
            }
        });



        startActivity(intent);
        finish();

    }


    private void showDialogForLogout(final int logoutOrclose) {

        final Dialog dialog = new Dialog(DashboardActivity.this);
        dialog.setContentView(R.layout.custom_logout);

        TextView txtmessage = (TextView) dialog.findViewById(R.id.txtmessage);
        TextView txtmessage_heading = (TextView) dialog.findViewById(R.id.txtmessage_heading);
        Button no = (Button) dialog.findViewById(R.id.btnstate1);
        Button yes = (Button) dialog.findViewById(R.id.btnsate2);
        if (logoutOrclose == 1) {
            txtmessage.setText(getResources().getString(R.string.closeapp));
        } else {
            txtmessage.setText(getResources().getString(R.string.surelogout));
        }

        dialog.show();
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (logoutOrclose == 1) {
                    finish();

                } else {
                    onLogoutSuccess();

                }

                dialog.dismiss();
            }
        });

        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }


    private void setConnectionLayout() {
        beforeConnection.setVisibility(View.VISIBLE);
        afterConnection.setVisibility(View.GONE);
    }

    private void testIPConnection() {
        showCustomProgressBar();

        try {
            Log.i("volley", "testIPConnectivityForScan " + EnvironmentUrl.testIPConnectivityForScan());
            BaseActivity.VolleyCallback callback = new BaseActivity.VolleyCallback() {
                @Override
                public void onSuccess(String result) {

                    try {
                        ToastShort(DashboardActivity.this, "Connected Successfully");
                        sharedPref.saveIPAddress(FXMConstants.SCAN_IP_VALUE);
                        //FXMConstants.SCAN_IP_VALUE = ipAddr;
                        showSuccessfullConnection();
                        Log.i("volley", "testIPConnectivityForScan - result : \n" + result.toString());
                        hideCustomProgressBar();
                        sendGCMCredentialToServer();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onError(String result) {
                    Log.i("volley", "testIPConnectivityForScan - error : \n" + result.toString());
                    //postScannetData("");
                    sharedPref.saveIPAddress(FXMConstants.SCAN_IP_VALUE);
                    ToastShort(DashboardActivity.this, "Server Connection Failed !!Please Enter Proper IP");
                    hideCustomProgressBar();
                }
            };
            volleyGetStringRequest(EnvironmentUrl.testIPConnectivityForScan(), callback);
        } catch (Exception t) {
            t.printStackTrace();
            hideCustomProgressBar();
            ToastShort(DashboardActivity.this, "Server Connection Failed !!Please Enter Proper IP");
        }
    }

    private void showSuccessfullConnection() {

        beforeConnection.setVisibility(View.GONE);
        afterConnection.setVisibility(View.VISIBLE);
        accesstoken.setText(sharedStorage.getGCMRegIdFrmSharedPrefs(DashboardActivity.this));

       /* if(CommonFunctions.isEmpty(FXMConstants.SCAN_IP_VALUE)){
            if(!CommonFunctions.isEmpty(sharedPref.getIPAddress())){
                FXMConstants.SCAN_IP_VALUE = sharedPref.getIPAddress();
                tv_ipval.setText(FXMConstants.SCAN_IP_VALUE);
            }else{
                setConnectionLayout();
            }
        }else{*/
        tv_ipval.setText(FXMConstants.SCAN_IP_VALUE);
        //}


    }

    private void showCustomProgressBar() {
        if (customProgressBar == null)
            customProgressBar = new CustomProgressBar(DashboardActivity.this);

        if (!customProgressBar.isProgressBarShowing()) {
            customProgressBar.showCustomDialog();
            customProgressBar.setCustomCancelable(true);
            customProgressBar.setCustomMessage(getResources().getString(R.string.loading));
        }
    }

    private void hideCustomProgressBar() {
        if (customProgressBar != null) {
            if (customProgressBar.isProgressBarShowing()) {
                customProgressBar.closeCustomDialog();
            }
        }
    }


    private void sendGCMCredential_Dock() {
        try {
            JSONObject data = new JSONObject();
            data.put("HotelCode", sharedStorage.getPMSCustCode());

            data.put("EmailID", sharedStorage.getUserId());
            data.put("GCMKey", sharedStorage.getGCMRegIdFrmSharedPrefs(DashboardActivity.this));
            String param = data.toString();

            data.put("EmailID", sharedStorage.getUserId());
            data.put("GCMKey", sharedStorage.getGCMRegIdFrmSharedPrefs(DashboardActivity.this));
            Log.i("FMCID", ">>> :" + sharedStorage.getGCMRegIdFrmSharedPrefs(DashboardActivity.this));
            String param1 = data.toString();

            VolleyCallback callback = new VolleyCallback() {
                @Override
                public void onSuccess(String result) {

                    String status = "";
                    String errorMsg = "";
                    CommonFunctions.showLog(TAG, "i", "forget password response : " + result);
                    try {
                        if (!CommonFunctions.isEmpty(result)) {
                            JSONObject jsonData = new JSONObject(result);
                            status = jsonData.optString("Status", "");
                            errorMsg = jsonData.optString("StatusDescription", "");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {

                    }
                }

                @Override
                public void onError(String result) {

                    //Toast.makeText(mContext, "Could not register FCM details to server", Toast.LENGTH_LONG).show();
                }
            };

            volleyPostRequest(EnvironmentUrl.setFCM_message_key(), param, callback);
        } catch (Exception f) {
        }
    }


    private void sendGCMCredentialToServer() {
        try {
            FCMParam data = new FCMParam();
            data.setPMSCustCode(sharedStorage.getPMSCustCode());
            data.setLoginID(sharedStorage.getUserId());
            data.setSource("FX-FOM");
            data.setDeviceID(sharedStorage.getGCMRegIdFrmSharedPrefs(DashboardActivity.this));
            data.setPlatform("Android");
            data.setPlatformVersion("" + Build.VERSION.SDK_INT);
            data.setAPPVersion("" + CommonFunctions.getAppVersion(DashboardActivity.this));
            data.setIsOnline("Login");

            Gson gson = new Gson();
            String jsonParam = gson.toJson(data);

            Log.i("FMCID", ">>> :" + sharedStorage.getGCMRegIdFrmSharedPrefs(DashboardActivity.this));
            Log.i("FCMLOG", jsonParam);

            VolleyCallback callback = new VolleyCallback() {
                @Override
                public void onSuccess(String result) {
                    try {
                        if (!CommonFunctions.isEmpty(result)) {
                            JSONObject jsonData = new JSONObject(result);
                            String status = jsonData.optString("Status", "");
                            String errorMsg = jsonData.optString("StatusDescription", "");
                            Log.e("FCM", "success updated gcmid into server");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onError(String result) {

                    Log.e("FCM", "could not register into server");
                }
            };

            volleyPostRequest(EnvironmentUrl.getFCMLogURL(), jsonParam, callback);
        } catch (Exception f) {
        }
    }


    @Override
    public void onBackPressed() {
        showDialogForLogout(1);
    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

        menu.add(0, v.getId(), 0, "Copy");
        menu.setHeaderTitle("Copy text"); //setting header title for menu
        TextView textView = (TextView) v;
        ClipboardManager manager = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
        ClipData clipData = ClipData.newPlainText("text", textView.getText());
        manager.setPrimaryClip(clipData);
        super.onCreateContextMenu(menu, v, menuInfo);
    }


    public void startService() {
        Intent serviceIntent = new Intent(this, AppActivatingService.class);
        serviceIntent.putExtra("inputExtra", "start");
        ContextCompat.startForegroundService(this, serviceIntent);
    }

    public void stopService(View v) {
        Intent serviceIntent = new Intent(this, AppActivatingService.class);
        stopService(serviceIntent);
    }


    public void sendIpReconnectMail() {
        Thread sender = new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    GmailSender sender = new GmailSender("demoidsnext@gmail.com", "demoids@1234");
                    sender.sendMail("IP Newly Connected , DeviceID ::",
                            sharedStorage.getGCMRegIdFrmSharedPrefs(DashboardActivity.this),
                            "demoidsnext@gmail.com",
                            "demoidsnext@gmail.com");
                    //dialog.dismiss();
                    //   ToastShort(getApplicationContext(), "Sent Mail Successfully");

                    Log.i(MyFirebaseMessagingService.class.getSimpleName(), "DeviceID sent successfully: " );

                } catch (Exception e) {
                    Log.i(MyFirebaseMessagingService.class.getSimpleName(), "Error sending DeviceID: " + e.getMessage());
                }
            }
        });
        sender.start();
    }


}
