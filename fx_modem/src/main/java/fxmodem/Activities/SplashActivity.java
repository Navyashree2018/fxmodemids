package fxmodem.Activities;


import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatDelegate;

import com.idsnext.fxmodem.R;

import fxmodem.Base.MyApplication;
import fxmodem.Gmail.GmailSender;
import fxmodem.Receivers.InternetConnectivityReceiver;
import fxmodem.fcm.MyFirebaseMessagingService;

import static fxmodem.Utils.FXMConstants.SPLASH_TIME_OUT;

public class SplashActivity extends BaseActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);

        if (!isTaskRoot()) {
            finish();
            return;
        }

        setContentView(R.layout.activity_splash_screen);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                if (sharedStorage.getLoginBoolean()) {
                    goToDashboardActivity();
                }else {
                    goToLoginActivity();
                }
            }
        }, SPLASH_TIME_OUT);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Intent.ACTION_BATTERY_CHANGED);


    }





    @Override
    public void onResume() {
        super.onResume();

       // MyApplication.getInstance().setConnectivityListener(this);
    }

    public void goToLoginActivity() {
        Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }




    private void sendInternetConnectivityMail() {

        Thread sender = new Thread(new Runnable() {
            @Override
            public void run() {
                try {


                    GmailSender sender = new GmailSender("demoidsnext@gmail.com", "demoids@1234");


                    sender.sendMail("Internet Disconnected : PMSInterface ",
                            "Internet discoonected from your device . Please connect",
                            "demoidsnext@gmail.com",
                            "demoidsnext@gmail.com");

                    //dialog.dismiss();
                    //   ToastShort(getApplicationContext(), "Sent Mail Successfully");

                    Log.i(MyFirebaseMessagingService.class.getSimpleName(), "battery mail sent successfully: " );

                } catch (Exception e) {
                    Log.i(MyFirebaseMessagingService.class.getSimpleName(), "batterry Error sending mail: " + e.getMessage());
                }
            }
        });
        sender.start();
    }




    public void goToDashboardActivity() {
        Intent intent = new Intent(SplashActivity.this, DashboardActivity.class);
        startActivity(intent);
        finish();
    }
}