package fxmodem.Activities;

import android.app.KeyguardManager;
import android.content.Context;
import android.content.Intent;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.RequiresApi;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.idsnext.fxmodem.R;

import org.json.JSONObject;

import javax.crypto.Cipher;

import fxmodem.Base.EnvironmentUrl;
import fxmodem.Models.LoginRequestResponse.Login;
import fxmodem.Models.LoginRequestResponse.UserAccessRights;
import fxmodem.Models.LoginRequestResponse.UserAccessRightsDetails;
import fxmodem.Utils.CommonFunctions;
import fxmodem.Utils.CustomProgressBar;
import fxmodem.Utils.FXMConstants;
import fxmodem.Utils.LocalSharedStorage;

public class LoginActivity extends BaseActivity {


    private CustomProgressBar customProgressBar;
    private static final String TAG = LoginActivity.class.getSimpleName();
    Gson gson = new Gson();

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        LocalSharedStorage pref = new LocalSharedStorage(getApplicationContext());


      /*  fingerprintImage = (ImageView) findViewById(R.id.fingerprintImage);

        fingerprintImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {







                             } });*/
        // added below line only for the live purpose
        //nvironmentUrl.setCurrentWorkingEnvironment("live");
        // need to change the logic
/*
        if (EnvironmentUrl.getCurrentWorkingEnvironment().equalsIgnoreCase("ENV_LIVE")){
            Intent intent = new Intent(LoginActivity.this,LiveLoginActivity.class);
            startActivity(intent);

        }*/


        // setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);


        initializeObject();
        initializeViews();

        sharedPref.saveIPAddress(FXMConstants.SCAN_IP_VALUE);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void initializeObject() {
        customProgressBar = new CustomProgressBar(LoginActivity.this);
        gson = new Gson();
    }

    private void initializeViews() {

        findViewById(R.id.btnProceed).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!isNetworkConnected()) {
                    ToastShort(LoginActivity.this, getResources().getString(R.string.no_internet));
                    return;
                }

                String email = ((EditText) findViewById(R.id.ed_loginemailid)).getText().toString().replace(" ", "").trim();
                String password = ((EditText) findViewById(R.id.ed_password)).getText().toString().replace(" ", "").trim();

                if (CommonFunctions.isEmpty(email)) {
                    Toast.makeText(LoginActivity.this, getResources().getString(R.string.empty_email), Toast.LENGTH_SHORT).show();
                } else if (!CommonFunctions.isValidEmail(email)) {
                    Toast.makeText(LoginActivity.this, getResources().getString(R.string.invalid_email), Toast.LENGTH_SHORT).show();
                } else if (CommonFunctions.isEmpty(password)) {
                    Toast.makeText(LoginActivity.this, getResources().getString(R.string.empty_password), Toast.LENGTH_SHORT).show();
                } /*else if (CommonFunctions.isEmpty(pmscustcode)) {
                    Toast.makeText(getBaseContext(), getResources().getString(R.string.empty_pmscustcode), Toast.LENGTH_SHORT).show();
                }*/ /*else if (CommonFunctions.isEmpty(selectedEnv)) {
                    // Need to remove this condition before live
                    Toast.makeText(getBaseConEtext(), "Select any environment", Toast.LENGTH_SHORT).show();
                } */ else {
                    sharedStorage.saveAccessToken("");
                    if (!isNetworkConnected()) {
                        ToastShort(LoginActivity.this, getResources().getString(R.string.no_internet));
                        return;
                    }


                   /* if (count==5){
                        findViewById(R.id.btnProceed).setBackgroundColor(getResources().getColor(R.color.colorGray));
                        showAlertDialog("Please try after 30 minutes", "overbooking");
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                findViewById(R.id.btnProceed).setBackgroundColor(getResources().getColor(R.color.colorAccent));
                                count=0;
                                //doLogin(email, password);

                            }
                        }, 1800000);

                    }else {*/
                    doLogin(email, password);


                }
            }

        });

        findViewById(R.id.txt_forgotPW).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // showForgotPWDialog();
            }
        });
    }



    private void doLogin(String userId, String password) {
        String Status = "true";


        Login loginParam = new Login();
        loginParam.setUserID(userId);
        loginParam.setPassword(password);
        loginParam.setProductCode(FXMConstants.PRODUCT_CODE);
        loginParam.setDeviceId(CommonFunctions.getUniqueDeviceId(LoginActivity.this));
        loginParam.setDeviceIP(CommonFunctions.getIPAddress(LoginActivity.this));

        String loginParamValue = gson.toJson(loginParam);

        CommonFunctions.showLog(TAG, "i", "user access rights url : " + loginParamValue);

        showCustomProgressBar();

        VolleyCallback callback = new VolleyCallback() {
            @Override
            public void onSuccess(String result) {
                UserAccessRightsDetails userAccessRightsDetails = null;
                String errorMsg = "";
                CommonFunctions.showLog(TAG, "i", "user access rights response : " + result);

                try {
                    if (!CommonFunctions.isEmpty(result)) {
                        JSONObject jsonResponse = new JSONObject(result);
                        String deviceStatus = jsonResponse.optString("Status", "");
                        errorMsg = jsonResponse.optString("StatusDescription", "");
                        if (deviceStatus != null && deviceStatus.length() > 0) {
                            if (deviceStatus.equalsIgnoreCase(FXMConstants.SUCCESS)) {
                                Gson gson = new Gson();

                                UserAccessRights userAccessRights = gson.fromJson(jsonResponse.toString(), UserAccessRights.class);
                                userAccessRightsDetails = userAccessRights.getResponse();

                                String liveToken = userAccessRightsDetails.getAccess_Token();


                                sharedStorage.saveLoginBoolen(true);
                                sharedStorage.saveAccessToken(liveToken);

                                hideCustomProgressBar();
                                goToDashBoardActivity();

                            } else {
                                hideCustomProgressBar();
                                Toast.makeText(getBaseContext(), errorMsg, Toast.LENGTH_SHORT).show();

                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {

                    if (userAccessRightsDetails.getDefaultHotel() != null && userAccessRightsDetails.getDefaultHotel().size() > 0) {
                        String defaultPmsCustCode = userAccessRightsDetails.getDefaultHotel().get(0).getHotelCode();
                        Log.i(TAG, "" + defaultPmsCustCode);
                        sharedStorage.savePMSCustCode(defaultPmsCustCode, "");

                    }
                    sharedStorage.saveUserId(userAccessRightsDetails.getUserDetails().getLoginID());



                }
            }

            @Override
            public void onError(String result) {
                hideCustomProgressBar();
                Toast.makeText(getBaseContext(), getResources().getString(R.string.invalid_login_userrights), Toast.LENGTH_SHORT).show();
            }
        };

        volleyPostRequest(EnvironmentUrl.getUserLoginData(), loginParamValue, callback);
    }

    public static String getUniqueDeviceId(Context ctx) {
        String androidId = "0";
        androidId = Settings.Secure.getString(ctx.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        Log.i("TAG", "Android id: " + androidId);
        return androidId;

    }

    private void showCustomProgressBar() {
        if (customProgressBar == null)
            customProgressBar = new CustomProgressBar(LoginActivity.this);

        if (!customProgressBar.isProgressBarShowing()) {
            customProgressBar.showCustomDialog();
            customProgressBar.setCustomCancelable(false);
            customProgressBar.setCustomMessage(getResources().getString(R.string.loading));
        }
    }

    private void hideCustomProgressBar() {
        if (customProgressBar != null) {
            if (customProgressBar.isProgressBarShowing()) {
                customProgressBar.closeCustomDialog();
            }
        }
    }

    public void goToDashBoardActivity() {
        Intent intent = new Intent(LoginActivity.this, DashboardActivity.class);
        CommonFunctions.isLogout = true;
        startActivity(intent);
        finish();
    }


}
