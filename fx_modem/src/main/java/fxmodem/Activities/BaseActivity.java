package fxmodem.Activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.JsonSyntaxException;
import com.idsnext.fxmodem.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import fxmodem.Base.MyApplication;
import fxmodem.Gmail.GmailSender;
import fxmodem.Utils.CommonFunctions;
import fxmodem.Utils.FXMConstants;
import fxmodem.Utils.LocalSharedStorage;
import fxmodem.fcm.MyFirebaseMessagingService;

public class BaseActivity extends AppCompatActivity {

    LocalSharedStorage sharedStorage;
    LocalSharedStorage sharedPref;
    private static final String TAG = BaseActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sharedStorage = new LocalSharedStorage(this);
        sharedPref = new LocalSharedStorage(BaseActivity.this);

    }


    @Override
    public void onResume() {
        super.onResume();
        if(isNetworkConnected()){
           // sendInternetConnectivityMail();
        }
    }


    private void sendInternetConnectivityMail() {

        Thread sender = new Thread(new Runnable() {
            @Override
            public void run() {
                try {


                    GmailSender sender = new GmailSender("demoidsnext@gmail.com", "demoids@1234");


                    sender.sendMail("Internet Disconnected : PMSInterface ",
                            "Internet discoonected from your device . Please connect",
                            "demoidsnext@gmail.com",
                            "demoidsnext@gmail.com");

                    //dialog.dismiss();
                    //   ToastShort(getApplicationContext(), "Sent Mail Successfully");

                    Log.i(MyFirebaseMessagingService.class.getSimpleName(), "battery mail sent successfully: " );

                } catch (Exception e) {
                    Log.i(MyFirebaseMessagingService.class.getSimpleName(), "batterry Error sending mail: " + e.getMessage());
                }
            }
        });
        sender.start();
    }



    public void volleyGetStringRequest(final String mUrl, final VolleyCallback callback) {
        if (!isNetworkConnected()) {

            ToastShort(this, getResources().getString(R.string.no_internet));
            callback.onError("No internet");
            return;
        }
        try {
            String tag_json_obj = "json_obj_req";
            //RequestQueue queue = Volley.newRequestQueue(this);
            // Request a string response from the provided URL.

            StringRequest stringRequest = new StringRequest(Request.Method.GET, mUrl,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            // Display the first 500 characters of the response string.
                            try {
                                Log.d(TAG, "response : " + response);
                                callback.onSuccess(response);
                            }  catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                    if (error != null) {
                        try {
                            Log.e(TAG, "VolleyError  - networkResponse.statusCode : " + error.networkResponse.statusCode);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    Log.e(TAG, "VolleyError  : " + error);
                    callback.onError(error.toString());
                }
            });

            //DiskBasedCache cache = new DiskBasedCache(getCacheDir(), 16 * 1024 * 1024);
            //queue = new RequestQueue(cache, new BasicNetwork(new HurlStack()));
            //queue.start();

            // clear all volley caches.
            //queue.add(new ClearCacheRequest(cache, null));
            int socketTimeout = FXMConstants.API_TIMEOUT;//50 seconds - change to what you want
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            stringRequest.setRetryPolicy(policy);
            // Adding request to request queue
            MyApplication.getInstance().addToRequestQueue(stringRequest, tag_json_obj);
            //queue.add(postRequest);
            Log.i(TAG, "volleyGetRequest - getRequest : " + stringRequest);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "volleyGetRequest - Exception : " + e.getMessage());
        }
    }



    public void volleyGetRequest(final String mUrl, final VolleyCallback callback) {
        if (!isNetworkConnected()) {
            ToastShort(this, getResources().getString(R.string.no_internet));
            callback.onError("No internet");
            return;
        }
        try {
            String tag_json_obj = "json_obj_req";
            //RequestQueue queue = Volley.newRequestQueue(this);
            JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, mUrl, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        Log.d(TAG, "response : " + response.toString());
                        CommonFunctions.printLog("i", "response", response.toString());
                        callback.onSuccess(response.toString());
                    } catch (JsonSyntaxException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            //Get response code here
                            error.printStackTrace();
                            if (error != null) {
                                try {
                                    Log.e(TAG, "VolleyError  - networkResponse.statusCode : " + error.networkResponse.statusCode);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                            Log.e(TAG, "VolleyError  : " + error);
                            callback.onError(error.toString());
                        }
                    }) {


                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                   // headers.put("Content-Type", "application/json; charset=utf-8");
                    //headers.put("Authorization", "Basic " + Base64.NO_WRAP);
                    headers.put("Authorization", "Bearer " + sharedStorage.getAccessToken());
                    Log.i("TAG", "Headers: " + headers.toString());
                    return headers;
                }
            };


            //DiskBasedCache cache = new DiskBasedCache(getCacheDir(), 16 * 1024 * 1024);
            //queue = new RequestQueue(cache, new BasicNetwork(new HurlStack()));
            //queue.start();

            // clear all volley caches.
            //queue.add(new ClearCacheRequest(cache, null));
            int socketTimeout = FXMConstants.API_TIMEOUT;//50 seconds - change to what you want
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            getRequest.setRetryPolicy(policy);
            // Adding request to request queue
            MyApplication.getInstance().addToRequestQueue(getRequest, tag_json_obj);
            //queue.add(postRequest);
            Log.i(TAG, "volleyGetRequest - getRequest : " + getRequest);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "volleyGetRequest - Exception : " + e.getMessage());
        }
    }



    public void volleyPostRequest(final String mUrl, final String jsonParams, final VolleyCallback callback) {
        if (!isNetworkConnected()) {
            ToastShort(this, getResources().getString(R.string.no_internet));
            callback.onError("No internet");
            return;
        }
        try {
            String tag_json_obj = "json_obj_req";
            //RequestQueue queue = Volley.newRequestQueue(this);
            JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, mUrl,
                    new JSONObject(jsonParams),
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                Log.e("VOLLEY", "\n\n\n*******************************************************************");
                                Log.i("VOLLEY", "\nURL:\n " + mUrl);
                                Log.i("VOLLEY", "\njsonParams: \n" + jsonParams);
                                Log.d("VOLLEY", "\nResponse :\n\n " + response.toString());
                                CommonFunctions.printLog("d", "\nResponse :\n\n ", response.toString());
                                callback.onSuccess(response.toString());
                            } catch (JsonSyntaxException e) {
                                e.printStackTrace();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            //Get response code here
                            error.printStackTrace();
                            if (error != null) {
                                try {
                                    Log.e("VOLLEY", "\n\n\n*******************************************************************");
                                    Log.i("VOLLEY", "\nURL:\n " + mUrl);
                                    Log.i("VOLLEY", "\njsonParams: \n" + jsonParams);
                                    Log.d("VOLLEY", "\nResponse :\n\n " + error.networkResponse.statusCode);
                                    Log.e("TAG", "VolleyError  - networkResponse.statusCode : " + error.networkResponse.statusCode + "\n mUrl: " + mUrl);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                            Log.e(TAG, "VolleyError  : " + error);
                            callback.onError(error.toString());
                        }
                    }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                  //  headers.put("Content-Type", "application/json; charset=utf-8");
//                  headers.put("Authorization", "Basic " + Base64.NO_WRAP);
                    //for token
                    headers.put("Authorization", "Bearer " + sharedStorage.getAccessToken());
                    Log.i("TAG", "Headers: " + headers.toString());
                    return headers;
                }
            };

            //DiskBasedCache cache = new DiskBasedCache(getCacheDir(), 16 * 1024 * 1024);
            //queue = new RequestQueue(cache, new BasicNetwork(new HurlStack()));
            //queue.start();

            // clear all volley caches.
            //queue.add(new ClearCacheRequest(cache, null));
            int socketTimeout = FXMConstants.API_TIMEOUT;//50 seconds - change to what you want
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postRequest.setRetryPolicy(policy);
            // Adding request to request queue
            MyApplication.getInstance().addToRequestQueue(postRequest, tag_json_obj);
            //queue.add(postRequest);

            Log.d(TAG, " - postRequest : " + postRequest);
            Log.d("param", jsonParams);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, " - Exception : " + e.getMessage());
            Log.d("param", jsonParams);
        }

    }

    public void volleyPutRequest(final String mUrl, final String jsonParams, final VolleyCallback callback) {
        if (!isNetworkConnected()) {
            ToastShort(this, getResources().getString(R.string.no_internet));
            callback.onError("No internet");
            return;
        }
        try {
            Log.d("json body ", jsonParams);
            String tag_json_obj = "json_obj_req";
            //RequestQueue queue = Volley.newRequestQueue(this);

            JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.PUT, mUrl,
                    new JSONObject(jsonParams),
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                Log.e("VOLLEY", "\n\n\n*******************************************************************");
                                Log.i("VOLLEY", "\nURL:\n " + mUrl);
                                Log.i("VOLLEY", "\njsonParams: \n" + jsonParams);
                                Log.d("VOLLEY", "\nResponse :\n\n " + response.toString());
                                CommonFunctions.printLog("d", "\nResponse :\n\n ", response.toString());
                                callback.onSuccess(response.toString());
                            } catch (JsonSyntaxException e) {
                                e.printStackTrace();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            //Get response code here
                            error.printStackTrace();
                            if (error != null) {
                                try {
                                    Log.e("VOLLEY", "\n\n\n*******************************************************************");
                                    Log.i("VOLLEY", "\nURL:\n " + mUrl);
                                    Log.i("VOLLEY", "\njsonParams: \n" + jsonParams);
                                    Log.d("VOLLEY", "\nResponse :\n\n " + error.networkResponse.statusCode);
                                    Log.e("TAG", "VolleyError  - networkResponse.statusCode : " + error.networkResponse.statusCode + "\n mUrl: " + mUrl);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                            Log.e(TAG, "VolleyError  : " + error);
                            callback.onError(error.toString());
                        }
                    }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                  //  headers.put("Content-Type", "application/json; charset=utf-8");
                    // headers.put("Authorization", "Basic " + Base64.NO_WRAP);
                    headers.put("Authorization", "Bearer " + sharedStorage.getAccessToken());
                    Log.i("TAG", "Headers: " + headers.toString());

                    return headers;
                }
            };
            //DiskBasedCache cache = new DiskBasedCache(getCacheDir(), 16 * 1024 * 1024);
            //queue = new RequestQueue(cache, new BasicNetwork(new HurlStack()));
            //queue.start();

            // clear all volley caches.
            //queue.add(new ClearCacheRequest(cache, null));
            int socketTimeout = FXMConstants.API_TIMEOUT;//50 seconds - change to what you want
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postRequest.setRetryPolicy(policy);
            // Adding request to request queue
            MyApplication.getInstance().addToRequestQueue(postRequest, tag_json_obj);
            //queue.add(postRequest);

            Log.d(TAG, "volleyPostRequest - postRequest : " + postRequest);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "volleyPostRequest - Exception : " + e.getMessage());
        }

    }



    public boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }


    public interface VolleyCallback {
        void onSuccess(String result) throws JSONException;

        void onError(String result);
    }
    public interface VolleyCallback1 {
        void onSuccess(String result) throws JSONException;

        void onError(VolleyError result);
    }

    public static void ToastLong(Context ctx, String msg) {
        MyApplication application = new MyApplication();
        try {
            Log.i("CRASH", ">>> toast long >> baseactivity");
            if (ctx != null)
                Toast.makeText(ctx, msg, Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void ToastShort(Context ctx, String msg) {

        MyApplication application = new MyApplication();
        try {
            Log.i("CRASH", ">>> toast short >> baseactivity");
            if (ctx != null)
                Toast.makeText(ctx, msg, Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


   /* private void checkConnectivity() {

        ConnectivityManager connec
                =(ConnectivityManager)getSystemService(getBaseContext().CONNECTIVITY_SERVICE);

        // Check for network connections
        if ( connec.getNetworkInfo(0).getState() ==
                android.net.NetworkInfo.State.CONNECTED ||
                connec.getNetworkInfo(0).getState() ==
                        android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() ==
                        android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED ) {
            Toast.makeText(this, " Connected ", Toast.LENGTH_LONG).show();
            sendInternetConnectivityPMSMail();
        }else if (
                connec.getNetworkInfo(0).getState() ==
                        android.net.NetworkInfo.State.DISCONNECTED ||
                        connec.getNetworkInfo(1).getState() ==
                                android.net.NetworkInfo.State.DISCONNECTED  ) {
            Toast.makeText(this, " Not Connected ", Toast.LENGTH_LONG).show();
        }

    }

    private void sendInternetConnectivityPMSMail() {

        Thread sender = new Thread(new Runnable() {
            @Override
            public void run() {
                try {


                    GmailSender sender = new GmailSender("demoidsnext@gmail.com", "demoids@1234");

                    sender.sendMail("Internet Disconnected : PMSInterface ",
                            "Internet discoonected from your device . Please connect",
                            "demoidsnext@gmail.com",
                            "demoidsnext@gmail.com");


                    Log.i(MyFirebaseMessagingService.class.getSimpleName(), "battery mail sent successfully: " );

                } catch (Exception e) {
                    Log.i(MyFirebaseMessagingService.class.getSimpleName(), "batterry Error sending mail: " + e.getMessage());
                }
            }
        });
        sender.start();
    }*/
}
