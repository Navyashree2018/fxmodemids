package fxmodem.Activities;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.idsnext.fxmodem.R;

import java.util.ArrayList;

import fxmodem.db.LogDBManger;
import fxmodem.db.LogDBModel;

public class LogDBActivity extends Activity {

    public String FXApiFirstLog = "";
    public String FXRequestFirstLog = "";
    public String FXResponseFirstLog = "";
    public String PmsApiLog = "";
    public String PmsRequestLog = "";
    public String PmsResponseLog = "";
    public String FXApiLastLog = "";
    public String FXRequestLastLog = "";
    public String FXResponseLastLog = "";

    Button btn_back;
    private LinearLayoutManager logListManager;
    private LogListView LogListViewAdapter;
    private RecyclerView rv_log;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log);
        
        btn_back = findViewById(R.id.btn_back);
        rv_log = findViewById(R.id.rv_fromroom);

        LogDBManger logDBManger = new LogDBManger(getApplicationContext());
        ArrayList<LogDBModel> logDBModelList = logDBManger.getLog();

        if (logDBModelList.size() == 0) {
            Toast.makeText(this, "No Record Exist", Toast.LENGTH_SHORT).show();
            finish();
        }

        if(logDBModelList.size() > 0) {
            logListManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
            LogListViewAdapter = new LogListView(logDBModelList);
            rv_log.setAdapter(LogListViewAdapter);
            rv_log.setLayoutManager(logListManager);
        }

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


/*
        for (LogDBModel logDBModel : logDBModelList) {

            FXApiFirstLog = logDBModel.getFXApiFirstLog();
            FXRequestFirstLog = logDBModel.getFXRequestFirstLog();
            FXResponseFirstLog = logDBModel.getFXResponseFirstLog();
            PmsApiLog = logDBModel.getPmsApiLog();
            PmsRequestLog = logDBModel.getPmsRequestLog();
            PmsResponseLog = logDBModel.getPmsResponseLog();
            FXApiLastLog = logDBModel.getFXApiLastLog();
            FXRequestLastLog = logDBModel.getFXRequestLastLog();
            FXResponseLastLog = logDBModel.getFXResponseLastLog();


            tv1.setText(FXApiFirstLog);
            tv2.setText(FXRequestFirstLog);
            tv3.setText(FXResponseFirstLog);

            tv4.setText(PmsApiLog);
            tv5.setText(PmsRequestLog);
            tv6.setText(PmsResponseLog);

            tv7.setText(FXApiLastLog);
            tv8.setText(FXRequestLastLog);
            tv9.setText(FXResponseLastLog);
        }
*/


    }


    class LogListView extends RecyclerView.Adapter<LogListView.ViewHolder> {

        ArrayList<LogDBModel> logList = new ArrayList<>();

        LogListView(ArrayList<LogDBModel> logs) {
            this.logList = logs;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_logs, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {


            FXApiFirstLog = logList.get(position).getFXApiFirstLog();
            FXRequestFirstLog = logList.get(position).getFXRequestFirstLog();
            FXResponseFirstLog = logList.get(position).getFXResponseFirstLog();
            PmsApiLog = logList.get(position).getPmsApiLog();
            PmsRequestLog = logList.get(position).getPmsRequestLog();
            PmsResponseLog = logList.get(position).getPmsResponseLog();
            FXApiLastLog = logList.get(position).getFXApiLastLog();
            FXRequestLastLog = logList.get(position).getFXRequestLastLog();
            FXResponseLastLog = logList.get(position).getFXResponseLastLog();


            if(FXApiFirstLog.equalsIgnoreCase("--")){

                holder.apidata.setVisibility(View.GONE);
                holder.notificationRecvData.setVisibility(View.VISIBLE);
                holder.tvnotification.setText(PmsApiLog + "\n"+ PmsRequestLog);
            }else{
                holder.apidata.setVisibility(View.VISIBLE);
                holder.notificationRecvData.setVisibility(View.GONE );

            }

            holder.tv1.setText(FXApiFirstLog);
            holder.tv2.setText(FXRequestFirstLog);
            holder.tv3.setText(FXResponseFirstLog);

            holder.tv4.setText(PmsApiLog);
            holder.tv5.setText(PmsRequestLog);
            holder.tv6.setText(PmsResponseLog);

            holder.tv7.setText(FXApiLastLog);
            holder.tv8.setText(FXRequestLastLog);
            holder.tv9.setText(FXResponseLastLog);

            holder.tv1.setVisibility(View.GONE);
            holder.tv2.setVisibility(View.GONE);
            holder.tv3.setVisibility(View.GONE);



        }


        @Override
        public int getItemCount() {
            return logList.size();

        }

        class ViewHolder extends RecyclerView.ViewHolder {
            TextView tv1, tv2, tv3, tv4, tv5, tv6, tv7, tv8, tv9 , tvnotification;
            LinearLayout notificationRecvData , apidata ;

            public ViewHolder(View itemView) {
                super(itemView);
                notificationRecvData = itemView.findViewById(R.id.ll_notification);
                apidata = itemView.findViewById(R.id.apiData);
                tvnotification = itemView.findViewById(R.id.tvNotification);


                tv1 = itemView.findViewById(R.id.tv1);
                tv2 = itemView.findViewById(R.id.tv2);
                tv3 = itemView.findViewById(R.id.tv3);

                tv4 = itemView.findViewById(R.id.tv4);
                tv5 = itemView.findViewById(R.id.tv5);
                tv6 = itemView.findViewById(R.id.tv6);

                tv7 = itemView.findViewById(R.id.tv7);
                tv8 = itemView.findViewById(R.id.tv8);
                tv9 = itemView.findViewById(R.id.tv9);

                tv1.setTextIsSelectable(true);
                registerForContextMenu(tv1);

                tv2.setTextIsSelectable(true);
                registerForContextMenu(tv2);

                tv3.setTextIsSelectable(true);
                registerForContextMenu(tv3);

                tv4.setTextIsSelectable(true);
                registerForContextMenu(tv4);

                tv5.setTextIsSelectable(true);
                registerForContextMenu(tv5);

                tv6.setTextIsSelectable(true);
                registerForContextMenu(tv6);

                tv7.setTextIsSelectable(true);
                registerForContextMenu(tv7);

                tv8.setTextIsSelectable(true);
                registerForContextMenu(tv8);

                tv9.setTextIsSelectable(true);
                registerForContextMenu(tv9);






            }
        }

    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

        menu.add(0, v.getId(),0, "Copy");
        menu.setHeaderTitle("Copy text"); //setting header title for menu
        TextView textView = (TextView) v;
        ClipboardManager manager = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
        ClipData clipData = ClipData.newPlainText("text", textView.getText());
        manager.setPrimaryClip(clipData);
        super.onCreateContextMenu(menu, v, menuInfo);
    }

}
