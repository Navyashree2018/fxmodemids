package fxmodem.Utils;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.wifi.WifiManager;
import android.provider.Settings;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import android.text.format.Formatter;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;

public class CommonFunctions {


    public static boolean isLogout = false;

    public static boolean isEmpty(String text) {
        return (text == null || text.length() == 0 || text.equalsIgnoreCase("null")) ? true : false;
    }


    public static void printLog(String logType, String className, String msg) {
        if (FXMConstants.ISDEBUG) {
            switch (logType) {
                case "v":
                    Log.v(className, msg);
                    break;
                case "i":
                    Log.i(className, msg);
                    break;
                case "e":
                    Log.e(className, msg);
                    break;
                case "d":
                    Log.d(className, msg);
                    break;
                default:
                    Log.v(className, msg);
            }
        }
    }

    public static String getIPAddress(Context ctx) {
        WifiManager wm = (WifiManager) ctx.getSystemService(Context.WIFI_SERVICE);
        String ip = Formatter.formatIpAddress(wm.getConnectionInfo().getIpAddress());
        return ip;
    }

    public static String getUniqueDeviceId(Context ctx) {
        String androidId = "0";
        androidId = Settings.Secure.getString(ctx.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        Log.i("TAG", "Android id: " + androidId);
        return androidId;

    }


    public static void showLog(String tag, String type, String msg) {
        try {
            if (FXMConstants.SHOW_LOG) {
                switch (type) {
                    case "i":
                        Log.i(tag, "" + msg);
                        break;
                    case "d":
                        Log.d(tag, "" + msg);
                        break;
                    case "e":
                        Log.e(tag, "" + msg);
                        break;
                    case "w":
                        Log.w(tag, "" + msg);
                        break;
                    case "v":
                        Log.v(tag, "" + msg);
                        break;
                    default:
                        Log.v(tag, "" + msg);
                        break;
                }
            }
        } catch (Exception e) {
            Log.v(tag, "" + msg);
        }
    }

    public static boolean isValidEmail(String email) {
        String emailRegex =
                "^[a-zA-Z0-9_+&*-]+(?:\\." +
                        "[a-zA-Z0-9_+&*-]+)*@" +
                        "(?:[a-zA-Z0-9-]+\\.)+[a-z" +
                        "A-Z]{2,7}$";
        Pattern pat = Pattern.compile(emailRegex);
        boolean result = pat.matcher(email).matches();
        return result;
    }

    public static String getDateTime()
    {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        return dateFormat.format(date);
    }

    public static String getDate()
    {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        Date date = new Date();
        return dateFormat.format(date);
    }

    public static int getAppVersion(Context ctx) {
        int versionCode = 0;
        try {
            PackageInfo pInfo = ctx.getPackageManager().getPackageInfo(ctx.getPackageName(), 0);
            versionCode = pInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return versionCode;

    }
}
