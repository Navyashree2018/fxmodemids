package fxmodem.Utils

object FXMConstants {
    const val SPLASH_TIME_OUT = 2000
    const val API_TIMEOUT = 500000
    const val ERROR = "Failure"
    const val SUCCESS = "Success"
    const val PRODUCT_CODE = "100"
    const val MESSAGE_TYPE_UPDATE = "UPDATE_APP"

    @JvmField
    var SCAN_IP_VALUE = ""
    @JvmField
    var SHOW_LOG = true // need to change before going livePosted Amount list is not displayed in both Paid-out/Refund when Posted from Room Status.
    @JvmField
    var ISDEBUG = false
    @JvmField
    var ENV_DEV = "dev"
    @JvmField
    var ENV_QA = "qa"
    @JvmField
    var ENV_FAT = "fat"
    @JvmField
    var ENV_MOBILE = "mobile"
    @JvmField
    var ENV_UA = "ua"
    @JvmField
    var ENV_PMS = "pms"
    @JvmField
    var ENV_PMSSYNC = "pmssync"
    @JvmField
    var ENV_LIVE = "live"
    @JvmField
    var ENV_STAGING = "staging"
    @JvmField
    var ENV_STAGING_SECOND = "staging_second"
    var TAG_CONTACTUS = "Online support"

    /*************************** NEVER CHANGE - SERVER URL  */ // Note : If you change it for testing purposek, keep old url for backup purpose. Do not replace existing url.
    const val serverBaseDevURL = "http://fooperationsapidev.azurewebsites.net/V1.0.0/" // old -> "http://fooperationsapidev.azurewebsites.net/";
    const val serverBaseMobileURL = "http://fooperationsapimobile.azurewebsites.net/V1.0.0/" // old -> http://fooperationsapimobile.azurewebsites.net/
    const val serverBaseQAURL = "https://fooperationsapiqa.azurewebsites.net/V1.0.0/" // old -> https://fooperationsapiqa.azurewebsites.net/
    const val serverBaseUAURL = "http://fooperationsapiua.azurewebsites.net/V1.0.0/"
    const val serverBaseDemoURL = "http://fooperationsapidemo.azurewebsites.net/"
    const val serverBaseFatURL = "http://fooperationsapifat.azurew7ebsites.net/"
    const val serverBasePMSURL = "http://fooperationsapipms.azurewebsites.net/V1.0.0/"
    const val serverBasePMSSYNCURL = " https://fooperationsapipmssync.azurewebsites.net/"
    const val serverBaseLiveURL = "https://fooperations.idsnext.com/V1.0.0/"
    const val serverBaseStagingURL = "http://fooperationsapistaging.azurewebsites.net/V1.0.0/"
    const val serverBaseStagingSecondURL = "https://fxfoapistg.azurewebsites.net/V1.0.0/"
    /** */
    /****************** CHANGE IT  */ // Replace this variable like ENV_DEV, ENV_UA,.etc >>> DO NOT replace like "qa"
    @JvmField
    val SERVER_ENV = ENV_LIVE
    var wifisatatus = false
}