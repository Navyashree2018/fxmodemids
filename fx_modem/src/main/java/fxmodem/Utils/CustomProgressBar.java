package fxmodem.Utils;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.idsnext.fxmodem.R;

import fxmodem.Activities.LoginActivity;

public class CustomProgressBar {
    //ProgressDialog progressDialog;
    AlertDialog progressDialog;
    Context mContext;
    TextView customText;

	/*public CustomProgressBar(Context ctx) {
		progressDialog = new ProgressDialog(ctx, ProgressDialog.THEME_DEVICE_DEFAULT_LIGHT);
		progressDialog.setCanceledOnTouchOutside(false);
		progressDialog.setCancelable(true);
	}*/

    public CustomProgressBar(Context ctx) {
        mContext = ctx;
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(mContext);
        final View dialogView = LayoutInflater.from(mContext).inflate(R.layout.customprogressbar, null);
        customText = (TextView) dialogView.findViewById(R.id.customProgressbarText);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(true);
       /* RelativeLayout mainLayout = (RelativeLayout) dialogView.findViewById(R.id.mainLayout);
        mainLayout.setBackgroundResource(R.drawable.my_progress_one);
        AnimationDrawable frameAnimation = (AnimationDrawable)mainLayout.getBackground();
        frameAnimation.start();
*/
        progressDialog = dialogBuilder.create();
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(true);
    }

    public void showCustomDialog() {
        try {
            if (progressDialog != null && !progressDialog.isShowing()) {
                progressDialog.show();
            }
        } catch (Exception e) {
        }
    }

    public void setCustomMessage(String msg) {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.setMessage(msg);
            if (CommonFunctions.isEmpty(msg)) {
                customText.setText("loading");

            } else {
                customText.setText(msg);
            }
        }
    }

    public void closeCustomDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    public void setCustomCancelable(boolean value) {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.setCancelable(value);
        }
    }

    public boolean isProgressBarShowing() {
        return ((progressDialog != null && progressDialog.isShowing()) ? true : false);
    }
}


