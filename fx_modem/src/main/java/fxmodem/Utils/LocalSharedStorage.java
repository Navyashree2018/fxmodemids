package fxmodem.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.firebase.iid.FirebaseInstanceId;

public class LocalSharedStorage {

    SharedPreferences preferences;
    private static final String PREF_TOKEN = "PREF_TOKEN";
    public Boolean PREF_LOGIN_CHECK = Boolean.valueOf("PREF_LOGIN_CHECK");
    public  static  final String PREF_IP_VALUE = "IP_ADDRESS";
    private static final String PREF_GCM_TOKEN = "GCM_TOKEN";
    private static final String PREF_DEFAULT_PMSCUSTCODE_KEY = "PMSCUSTCODE";
    private static final String PREF_DEFAULT_PROPACCDATE_KEY = "PROPERTYACCTDATE";
    private static final String PREF_USERID_KEY = "USERID";


    String gcmId = "";

    public LocalSharedStorage(Context ctx) {

        try {
            preferences = PreferenceManager.getDefaultSharedPreferences(ctx);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }



    public void saveLoginBoolen(Boolean name) {


        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(String.valueOf(PREF_LOGIN_CHECK), name);
        editor.commit();
    }



    public Boolean getLoginBoolean() {
        Boolean code = preferences.getBoolean(String.valueOf(PREF_LOGIN_CHECK), false);
        return code;
    }

    public void saveAccessToken(String name) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(PREF_TOKEN, name);
        editor.commit();
    }

    public String getAccessToken() {
        String code = preferences.getString(PREF_TOKEN, "");
        return code;
    }


    public void saveIPAddress(String ip_value) {


        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(PREF_IP_VALUE, ip_value);
        editor.commit();
    }

    public void ClearIPValue() {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(PREF_IP_VALUE, "");
        editor.commit();
    }

    public String getIPAddress() {
        String code = preferences.getString(PREF_IP_VALUE, "");
        return code;
    }

    public void save_GCM_Token(String token) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(PREF_GCM_TOKEN, token);
        editor.commit();
    }

    public String getgcm_token() {
        String code = preferences.getString(PREF_GCM_TOKEN, "");
        return code;
    }

    /**
     * method to get GCM id from device from saved file and send it to server
     */
    public String getGCMRegIdFrmSharedPrefs(Context ctx) {

        try {
            gcmId = getgcm_token();

            if (gcmId.isEmpty() || gcmId.length() < 5) {
                if (checkPlayServices(ctx)) {
                    registerInBackground(ctx);
                }
            } else {
                Log.i("GCM","gcm key value in preference 1"+gcmId);
                save_GCM_Token(gcmId);
            }
        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        }
        return gcmId;
    }


    /**
     * method to check google service is installed or not because if it is not active or not intalled it will fail to generate FCM token
     */
    public static boolean checkPlayServices(Context ctx) {
        try {
            GoogleApiAvailability googleAPI = GoogleApiAvailability.getInstance();
            int resultCode = googleAPI.isGooglePlayServicesAvailable(ctx);
            if (resultCode != ConnectionResult.SUCCESS) {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }


    /**
     * method to reget GCM token if not available for FCM message
     */
    private void registerInBackground(final Context ctx) {
        try {
            new AsyncTask<Void, Void, String>() {
                @Override
                protected String doInBackground(Void... params) {
                    String gcmId = "";
                    try {
                        //get fcm token
                        gcmId = FirebaseInstanceId.getInstance().getToken();
                        Log.i("GCM","gcm key value in preference 2"+gcmId);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                    return gcmId;
                }

                @Override
                protected void onPostExecute(String gcmId) {
                    save_GCM_Token(gcmId);

                }
            }.execute(null, null, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getPMSCustCode() {
        Log.i("PMSCODE", "Default: " + preferences.getString(PREF_DEFAULT_PMSCUSTCODE_KEY, ""));
        return preferences.getString(PREF_DEFAULT_PMSCUSTCODE_KEY, "");
    }

    public void savePMSCustCode(String pmsCustCode, String accDate) {
        Log.i("PMSCODE", "savePropertyDetail: " + pmsCustCode);

        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(PREF_DEFAULT_PMSCUSTCODE_KEY, pmsCustCode);
        editor.putString(PREF_DEFAULT_PROPACCDATE_KEY, accDate);
        editor.commit();

    }


    public String getUserId() {
        return preferences.getString(PREF_USERID_KEY, "");
    }


    public void saveUserId(String value) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(PREF_USERID_KEY, value);
        editor.commit();
    }
}
