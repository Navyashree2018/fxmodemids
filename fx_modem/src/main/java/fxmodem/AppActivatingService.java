package fxmodem;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.IBinder;
import android.os.SystemClock;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.Toast;
import androidx.core.app.NotificationCompat;
import com.idsnext.fxmodem.R;
import fxmodem.Activities.DashboardActivity;
import fxmodem.Gmail.GmailSender;
import fxmodem.Receivers.InternetConnectivityReceiver;
import fxmodem.Utils.LocalSharedStorage;

import static fxmodem.Base.MyApplication.CHANNEL_ID;

public class AppActivatingService extends Service {

    Intent serviceIntent;
    private static BroadcastReceiver broadcastReceiver;
    LocalSharedStorage sharedStorage;

    public AppActivatingService() {
        sharedStorage = new LocalSharedStorage(this);

    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        serviceIntent = intent;
        Log.i(AppActivatingService.class.getSimpleName(), "started");
        Toast.makeText(getApplicationContext(), "Service running in Background",
                Toast.LENGTH_SHORT).show();

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);
        intentFilter.addAction(Intent.ACTION_BATTERY_CHANGED);

        this.registerReceiver(new InternetConnectivityReceiver(), intentFilter);

        String input = intent.getStringExtra("inputExtra");
        Intent notificationIntent = new Intent(this, DashboardActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,
                0, notificationIntent, 0);
        Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle("FxModem")
                .setContentText("FxModem Connected")
                .setAutoCancel(false)
                .setOngoing(true)
                .setSmallIcon(R.mipmap.fc_logo)
                .setContentIntent(pendingIntent)
                .build();



        RemoteViews remoteViews = new RemoteViews("com.idsnext.fxmodem", R.layout.notificationbar);
        Notification notificationbuilder = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setSmallIcon(R.mipmap.fc_logo)
                .setOngoing(true)
                .setPriority(2)
                .setAutoCancel(false)
                .setOngoing(true)
                .setContent(remoteViews)
                .build();

        startForeground(1, notification);
        return START_REDELIVER_INTENT;
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.i(AppActivatingService.class.getSimpleName(), "OnBind");
        throw new UnsupportedOperationException("Exception");
    }



    @Override
    public void onTaskRemoved(Intent rootIntent) {
        Intent restartServiceIntent = new Intent(getApplicationContext(), this.getClass());
        restartServiceIntent.setPackage(getPackageName());

        PendingIntent restartServicePendingIntent = PendingIntent.getService(getApplicationContext(), 1, restartServiceIntent, PendingIntent.FLAG_ONE_SHOT);
        AlarmManager alarmService = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        alarmService.set(
                AlarmManager.ELAPSED_REALTIME,
                SystemClock.elapsedRealtime() + 1000,
                restartServicePendingIntent);

        super.onTaskRemoved(rootIntent);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        ScreenOffReceiver();
        //registerScreenOffReceiver();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }


    private void ScreenOffReceiver()
    {
        IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_OFF);
        registerReceiver(new InternetConnectivityReceiver(), filter);
    }



    private void registerScreenOffReceiver()
    {
        broadcastReceiver = new BroadcastReceiver()
        {
            @Override
            public void onReceive(Context context, Intent intent)
            {
                ConnectivityManager cm = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
                boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
                Log.i("Service","connected?   "+isConnected);
                if(isConnected){
                    sendInternetConnectivityMail();
                }
            }
        };
        IntentFilter filter = new IntentFilter(WifiManager.SUPPLICANT_STATE_CHANGED_ACTION);
        registerReceiver(broadcastReceiver, filter);
    }


    private void sendInternetConnectivityMail() {
        Thread sender = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    GmailSender sender = new GmailSender("demoidsnext@gmail.com", "demoids@1234");
                    sender.sendMail("Internet connected : PMSInterface " + sharedStorage.getPMSCustCode(),
                            "Internet conected to  your device ",
                            "demoidsnext@gmail.com",
                            "demoidsnext@gmail.com");

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        sender.start();
    }


}

