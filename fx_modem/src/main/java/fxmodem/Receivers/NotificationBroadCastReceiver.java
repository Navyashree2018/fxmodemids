package fxmodem.Receivers;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import androidx.legacy.content.WakefulBroadcastReceiver;
import java.util.ArrayList;
import fxmodem.Gmail.GmailSender;
import fxmodem.Utils.LocalSharedStorage;
import fxmodem.db.LogDBManger;
import fxmodem.db.LogDBModel;
import fxmodem.fcm.MyFirebaseMessagingService;

public class NotificationBroadCastReceiver extends WakefulBroadcastReceiver {
    LocalSharedStorage sharedPref;
    String title , body , packageName;

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getExtras() != null) {
            for (String key : intent.getExtras().keySet()) {
                Object value = intent.getExtras().get(key);
                Log.d("JJJJJJJJJJJJJJJJJ", "Key: " + key + " Value: " + value);
            }
        }

        if(intent.getExtras().getString("gcm.notification.title")!=null)
           title = intent.getExtras().getString("gcm.notification.title");
        if(intent.getExtras().getString("gcm.notification.body")!=null)
           body = intent.getExtras().getString("gcm.notification.body");
        if(intent.getExtras().getString("collapse_key")!=null)
           packageName = intent.getExtras().getString("collapse_key");

        if(title == null) title = "";
        if(body == null) body = "";
        if(packageName == null) packageName = "";

        if(packageName.equalsIgnoreCase("com.idsnext.fxmodem")) {
            if ((body.equalsIgnoreCase("PrintLog")) || (title.equalsIgnoreCase("PrintLog")))
                sendLogMail(context);
        }
    }


    private void sendLogMail(final Context context) {
        sharedPref = new LocalSharedStorage(context);
        Thread sender = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    LogDBManger logDBManger = new LogDBManger(context);
                    ArrayList<LogDBModel> logDBModelList = logDBManger.getLog();
                    GmailSender sender = new GmailSender("demoidsnext@gmail.com", "demoids@1234");
                    sender.sendMail("Logs :"+sharedPref.getPMSCustCode()+"\n",
                            logDBModelList.toString()+"",
                            "demoidsnext@gmail.com",
                            "demoidsnext@gmail.com");

                    Log.i(MyFirebaseMessagingService.class.getSimpleName(), "Logs mail sent successfully: " );

                } catch (Exception e) {
                    Log.i(MyFirebaseMessagingService.class.getSimpleName(), "Logs Error sending mail: " + e.getMessage());
                }
            }
        });
        sender.start();
    }
}
