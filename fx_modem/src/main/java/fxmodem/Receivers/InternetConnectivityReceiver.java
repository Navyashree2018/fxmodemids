package fxmodem.Receivers;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.WifiManager;
import android.os.BatteryManager;
import android.util.Log;
import fxmodem.AppActivatingService;
import fxmodem.Gmail.GmailSender;
import fxmodem.Utils.LocalSharedStorage;
import fxmodem.fcm.MyFirebaseMessagingService;

import static androidx.legacy.content.WakefulBroadcastReceiver.startWakefulService;

public class InternetConnectivityReceiver extends BroadcastReceiver {

    boolean state5sent = false;
    boolean state4sent = false;
    boolean state3sent = false;
    boolean state2sent = false;
    boolean state1sent = false;

    LocalSharedStorage sharedStorage;

    private static final String TAG = "OurBroadcastRecvr";
    private Context ctx;
    private static boolean firstTime = true;
    private  static boolean mailSend = false;


    public InternetConnectivityReceiver() {
        super();
    }

    @Override
    public void onReceive(Context context, final Intent intent) {

        Log.i(TAG, "onReceive() Entered" +intent.getAction()+":::"+intent);

        ctx = context;
        sharedStorage = new LocalSharedStorage(context);

        ComponentName comp = new ComponentName(context.getPackageName(),
                MyFirebaseMessagingService.class.getName());
        startWakefulService(context, (intent.setComponent(comp)));
         setResultCode(Activity.RESULT_OK);

        IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = ctx.registerReceiver(null, ifilter);
        int level = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);


        if (level == 5 && state5sent == false) {
            state5sent = true;
            sendBatteryDrainMail(level);
        } else if (level == 4 && state4sent == false) {
            state4sent = true;
            sendBatteryDrainMail(level);
        } else if (level == 3 && state3sent == false) {
            state3sent = true;
            sendBatteryDrainMail(level);
        } else if (level == 2 && state3sent == false) {
            state2sent = true;
            sendBatteryDrainMail(level);
        } else if (level == 1 && state3sent == false) {
            state1sent = true;
            sendBatteryDrainMail(level);
        }


        if (level > 5) {
            state5sent = false;
            state4sent = false;
            state3sent = false;
            state2sent = false;
            state1sent = false;
        }


        if (intent.equals(Intent.ACTION_SCREEN_OFF)) {
            Log.i(TAG, "Broadcasted service started");
            context.startService(new Intent(context, AppActivatingService.class));

        }


        WifiManager cm = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        // NetworkInfo activeNetwork = cm.getWifiState();
//  boolean isConnected = activeNetwork != null && activeNetwork.isConnected();
        Log.i(TAG, "connected?   " + cm.getWifiState());
        if (cm.getWifiState() == WifiManager.WIFI_STATE_ENABLED && !mailSend) {
            firstTime = false;
            mailSend = true;
            sendInternetConnectivityMail();
        }
        else if(cm.getWifiState() != WifiManager.WIFI_STATE_ENABLED){
            mailSend = false;
        }
        else{
            firstTime = true;
        }
    }


    private void sendBatteryDrainMail(final int result) {
        Thread sender = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    GmailSender sender = new GmailSender("demoidsnext@gmail.com", "demoids@1234");
                    sender.sendMail("Battery Drain : PMSInterface :" + sharedStorage.getPMSCustCode(),
                            "Battery is Draining \t " + result + "%" + "available.Please Charge it .",
                            "demoidsnext@gmail.com",
                            "demoidsnext@gmail.com");


                    Log.i(TAG, "battery mail sent successfully: ");

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        sender.start();
    }


    private void sendInternetConnectivityMail() {
        Thread sender = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    GmailSender sender = new GmailSender("demoidsnext@gmail.com", "demoids@1234");
                    sender.sendMail("Internet connected : PMSInterface " + sharedStorage.getPMSCustCode(),
                            "Internet conected to  your device ",
                            "demoidsnext@gmail.com",
                            "demoidsnext@gmail.com");

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        sender.start();
    }


}

